import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:test_majoo/data/core/api_client.dart';
import 'package:test_majoo/data/core/dabatase_helper.dart';
import 'package:test_majoo/data/data_sources/auth_local_data_source.dart';
import 'package:test_majoo/data/data_sources/favorit_local_data_source.dart';
import 'package:test_majoo/data/data_sources/people_local_data_source.dart';
import 'package:test_majoo/data/data_sources/people_remote_data_source.dart';
import 'package:test_majoo/data/data_sources/user_local_data_source.dart';
import 'package:test_majoo/data/repositories/favorit_repository_impl.dart';
import 'package:test_majoo/data/repositories/people_repository_impl.dart';
import 'package:test_majoo/data/repositories/user_repository_impl.dart';
import 'package:test_majoo/domain/repositories/favorit_repository.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';
import 'package:test_majoo/domain/repositories/user_repository.dart';
import 'package:test_majoo/domain/usecases/check_user_login_case.dart';
import 'package:test_majoo/domain/usecases/create_favorit_case.dart';
import 'package:test_majoo/domain/usecases/create_people_case.dart';
import 'package:test_majoo/domain/usecases/delete_favorit_case.dart';
import 'package:test_majoo/domain/usecases/delete_people_case.dart';
import 'package:test_majoo/domain/usecases/favorit_people_case.dart';
import 'package:test_majoo/domain/usecases/get_detail_film_people_case.dart';
import 'package:test_majoo/domain/usecases/get_detail_people_case.dart';
import 'package:test_majoo/domain/usecases/get_detail_species_people_case.dart';
import 'package:test_majoo/domain/usecases/get_list_favorit_case.dart';
import 'package:test_majoo/domain/usecases/get_list_favorit_people_case.dart';
import 'package:test_majoo/domain/usecases/get_list_people_case.dart';
import 'package:test_majoo/domain/usecases/get_user_login_case.dart';
import 'package:test_majoo/domain/usecases/login_user_case.dart';
import 'package:test_majoo/domain/usecases/logout_user_case.dart';
import 'package:test_majoo/domain/usecases/register_user_case.dart';
import 'package:test_majoo/domain/usecases/update_people_case.dart';
import 'package:test_majoo/presentation/blocs/auth/auth_bloc.dart';
import 'package:test_majoo/presentation/blocs/dashboard/dashboard_bloc.dart';
import 'package:test_majoo/presentation/blocs/detail-people/detail_people_bloc.dart';
import 'package:test_majoo/presentation/blocs/favorit/favorit_bloc.dart';
import 'package:test_majoo/presentation/blocs/loading/loading_bloc.dart';
import 'package:test_majoo/presentation/blocs/login/login_bloc.dart';
import 'package:test_majoo/presentation/blocs/people/people_bloc.dart';
import 'package:test_majoo/presentation/blocs/register/register_bloc.dart';

final getItInstance = GetIt.I;

Future init() async {
  getItInstance.registerLazySingleton<Dio>(() => Dio(BaseOptions(
    connectTimeout: 20000,
    receiveTimeout: 20000,
    contentType: "application/json;charset=utf-8",
  )));
  injectDataSource();
  injectRepository();
  injectBloc();
  injectUseCase();
}

void injectUseCase() {
  getItInstance
      .registerLazySingleton<RegisterUserCase>(() => RegisterUserCase(getItInstance()));

  getItInstance
      .registerLazySingleton<LoginUserCase>(() => LoginUserCase(getItInstance()));

  getItInstance
      .registerLazySingleton<CheckUserLoginCase>(() => CheckUserLoginCase(getItInstance()));

  getItInstance
      .registerLazySingleton<GetListPeopleCase>(() => GetListPeopleCase(getItInstance()));

  getItInstance
      .registerLazySingleton<UpdatePeopleCase>(() => UpdatePeopleCase(getItInstance()));

  getItInstance
      .registerLazySingleton<GetListFavoritPeopleCase>(() => GetListFavoritPeopleCase(getItInstance()));

  getItInstance
      .registerLazySingleton<FavoritPeopleCase>(() => FavoritPeopleCase(getItInstance()));

  getItInstance
      .registerLazySingleton<GetListFavoritCase>(() => GetListFavoritCase(getItInstance()));

  getItInstance
      .registerLazySingleton<CreateFavoritCase>(() => CreateFavoritCase(getItInstance()));

  getItInstance
      .registerLazySingleton<DeleteFavoritCase>(() => DeleteFavoritCase(getItInstance()));

  getItInstance
      .registerLazySingleton<GetDetailPeopleCase>(() => GetDetailPeopleCase(getItInstance()));

  getItInstance
      .registerLazySingleton<CreatePeopleCase>(() => CreatePeopleCase(getItInstance()));

  getItInstance
      .registerLazySingleton<DeletePeopleCase>(() => DeletePeopleCase(getItInstance()));

  getItInstance
      .registerLazySingleton<GetUserLoginCase>(() => GetUserLoginCase(getItInstance()));

  getItInstance
      .registerLazySingleton<LogoutUserCase>(() => LogoutUserCase(getItInstance()));

  getItInstance
      .registerLazySingleton<GetDetailFilmPeopleCase>(() => GetDetailFilmPeopleCase(getItInstance()));

  getItInstance
      .registerLazySingleton<GetDetailSpeciesPeopleCase>(() => GetDetailSpeciesPeopleCase(getItInstance()));
}

void injectBloc() {
  getItInstance
      .registerFactory<LoadingBloc>(() => LoadingBloc());

  getItInstance
      .registerFactory<LoginBloc>(() => LoginBloc(getItInstance(), getItInstance()));

  getItInstance
      .registerFactory<RegisterBloc>(() => RegisterBloc(getItInstance(),getItInstance()));


  getItInstance
      .registerFactory<DashboardBloc>(() => DashboardBloc(getItInstance(),getItInstance(),getItInstance(), getItInstance(), getItInstance()));

  getItInstance
      .registerFactory<DetailPeopleBloc>(() => DetailPeopleBloc(getItInstance(),getItInstance(),getItInstance()));

  getItInstance
      .registerFactory<FavoritBloc>(() => FavoritBloc(getItInstance(),getItInstance()));

  getItInstance
      .registerFactory<AuthBloc>(() => AuthBloc(getItInstance(),getItInstance()));

  getItInstance
      .registerFactory<PeopleBloc>(() => PeopleBloc(getItInstance(),getItInstance(),getItInstance(),getItInstance(),getItInstance()));
}

void injectRepository() {
  getItInstance.registerLazySingleton<UserRepository>(
          () => UserRepositoryImpl(getItInstance(), getItInstance()));

  getItInstance.registerLazySingleton<PeopleRepository>(
          () => PeopleRepositoryImpl(getItInstance(), getItInstance(), getItInstance()));

  getItInstance.registerLazySingleton<FavoritRepository>(
          () => FavoritRepositoryImpl(getItInstance()));
}

void injectDataSource() {
  getItInstance
      .registerLazySingleton<ApiClient>(() => ApiClient(dio: getItInstance()));

  getItInstance
      .registerLazySingleton<DatabaseHelper>(() => DatabaseHelper());

  getItInstance
      .registerLazySingleton<UserLocalDataSource>(() => UserLocalDataSourceImpl(getItInstance()));


  getItInstance
      .registerLazySingleton<AuthenticationLocalDataSource>(() => AuthenticationLocalDataSourceImpl());


  getItInstance
      .registerLazySingleton<PeopleRemoteDataSource>(() => PeopleRemoteDataSourceImpl(getItInstance()));

  getItInstance
      .registerLazySingleton<PeopleLocalDataSource>(() => PeopleLocalDataSourceImpl(getItInstance()));

  getItInstance
      .registerLazySingleton<FavoritLocalDataSource>(() => FavoritLocalDataSourceImpl(getItInstance()));
}