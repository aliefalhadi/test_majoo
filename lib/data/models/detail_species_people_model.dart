// To parse this JSON data, do
//
//     final detailSpeciesPeopleModel = detailSpeciesPeopleModelFromJson(jsonString);

import 'dart:convert';

DetailSpeciesPeopleModel detailSpeciesPeopleModelFromJson(String str) => DetailSpeciesPeopleModel.fromJson(json.decode(str));

String detailSpeciesPeopleModelToJson(DetailSpeciesPeopleModel data) => json.encode(data.toJson());

class DetailSpeciesPeopleModel {
  DetailSpeciesPeopleModel({
    required this.name,
    required this.classification,
    required this.designation,
    required this.averageHeight,
    required this.skinColors,
    required this.hairColors,
    required this.eyeColors,
    required this.averageLifespan,
    this.homeworld,
    required this.language,
    this.people,
    this.films,
    required this.created,
    required this.edited,
    required this.url,
  });

  String name;
  String classification;
  String designation;
  String averageHeight;
  String skinColors;
  String hairColors;
  String eyeColors;
  String averageLifespan;
  dynamic homeworld;
  String language;
  List<String>? people;
  List<String>? films;
  DateTime created;
  DateTime edited;
  String url;

  factory DetailSpeciesPeopleModel.fromJson(Map<String, dynamic> json) => DetailSpeciesPeopleModel(
    name: json["name"],
    classification: json["classification"],
    designation: json["designation"],
    averageHeight: json["average_height"],
    skinColors: json["skin_colors"],
    hairColors: json["hair_colors"],
    eyeColors: json["eye_colors"],
    averageLifespan: json["average_lifespan"],
    homeworld: json["homeworld"],
    language: json["language"],
    people: List<String>.from(json["people"].map((x) => x)),
    films: List<String>.from(json["films"].map((x) => x)),
    created: DateTime.parse(json["created"]),
    edited: DateTime.parse(json["edited"]),
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "classification": classification,
    "designation": designation,
    "average_height": averageHeight,
    "skin_colors": skinColors,
    "hair_colors": hairColors,
    "eye_colors": eyeColors,
    "average_lifespan": averageLifespan,
    "homeworld": homeworld,
    "language": language,
    "people": List<dynamic>.from(people!.map((x) => x)),
    "films": List<dynamic>.from(films!.map((x) => x)),
    "created": created.toIso8601String(),
    "edited": edited.toIso8601String(),
    "url": url,
  };
}
