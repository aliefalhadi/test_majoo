const String tableFavorit = 'favorit';

class FavoritFields {
  static final List<String> values = [
    /// Add all fields
    idFavorit, idUser, idPeople
  ];

  static const String idFavorit = '_id_favorit';
  static const String idUser = 'id_user';
  static const String idPeople = 'id_people';
}

class Favorit {
  final int? id;
  final int idUser;
  final int idPeople;

  const Favorit({
    this.id,
    required this.idUser,
    required this.idPeople,
  });

  Favorit copy({
    int? id,
    int? idUser,
    int? idPeople,
    String? password,
    DateTime? createdTime,
  }) =>
      Favorit(
        id: id ?? this.id,
        idUser: idUser ?? this.idUser,
        idPeople: idPeople ?? this.idPeople,
      );

  static Favorit fromJson(Map<String, Object?> json) => Favorit(
    id: json[FavoritFields.idFavorit] as int?,
    idUser: json[FavoritFields.idUser] as int,
    idPeople: json[FavoritFields.idPeople] as int
  );

  Map<String, Object?> toJson() => {
    FavoritFields.idFavorit: id,
    FavoritFields.idUser: idUser,
    FavoritFields.idPeople: idPeople,
  };
}