const String tableNotes = 'users';

class UserFields {
  static final List<String> values = [
    /// Add all fields
    id, username, email, password, time
  ];

  static const String id = '_id';
  static const String username = 'username';
  static const String email = 'email';
  static const String password = 'password';
  static const String time = 'time';
}

class User {
  final int? id;
  final String username;
  final String email;
  final String password;
  final DateTime createdTime;

  const User({
    this.id,
    required this.username,
    required this.password,
    required this.email,
    required this.createdTime,
  });

  User copy({
    int? id,
    String? username,
    String? email,
    String? password,
    DateTime? createdTime,
  }) =>
      User(
        id: id ?? this.id,
        username: username ?? this.username,
        email: email ?? this.email,
        password: password ?? this.password,
        createdTime: createdTime ?? this.createdTime,
      );

  static User fromJson(Map<String, Object?> json) => User(
    id: json[UserFields.id] as int?,
    username: json[UserFields.username] as String,
    email: json[UserFields.email] as String,
    password: json[UserFields.password] as String,
    createdTime: DateTime.parse(json[UserFields.time] as String),
  );

  Map<String, Object?> toJson() => {
    UserFields.id: id,
    UserFields.username: username,
    UserFields.email: email,
    UserFields.password: password,
    UserFields.time: createdTime.toIso8601String(),
  };
}