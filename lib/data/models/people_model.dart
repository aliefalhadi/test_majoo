import 'dart:convert';

const String tablePeople = 'people';

class PeopleFields {
  static final List<String> values = [
    /// Add all fields
    id,  name, height, mass, hairColor, skinColor, eyeColor,
    birthYear, gender,
    films, species, url, created, edited
  ];

  static const String id = '_id';
  static const String name = 'name';
  static const String height = 'height';
  static const String mass = 'mass';
  static const String hairColor = 'hair_color';
  static const String skinColor = 'skin_color';
  static const String eyeColor = 'eye_color';
  static const String birthYear = 'birth_year';
  static const String gender = 'gender';
  static const String films = 'films';
  static const String species = 'species';
  static const String url = 'url';
  static const String created = 'created';
  static const String edited = 'edited';
}

class People {
  final int? id;
  final String name;
  final String height;
  final String mass;
  final String hairColor;
  final String skinColor;
  final String eyeColor;
  final String birthYear;
  final String gender;
  final List<String>? films;
  final List<String>? species;
  final String url;
  final DateTime created;
  final DateTime edited;

  const People(
      {this.id,
      required this.name,
      required this.height,
      required this.mass,
      required this.hairColor,
      required this.skinColor,
      required this.eyeColor,
      required this.birthYear,
      required this.gender,
      required this.films,
      required this.species,
      required this.url,
      required this.created,
      required this.edited});

  People copy({
    int? id,
    String? name,
    String? height,
    String? mass,
    String? hairColor,
    String? skinColor,
    String? eyeColor,
    String? birthYear,
    String? gender,
    List<String>? films,
    List<String>? species,
    String? url,
    DateTime? created,
    DateTime? edited,
  }) =>
      People(
        id: id ?? this.id,
        name: name ?? this.name,
        height: height ?? this.height,
        mass: mass ?? this.mass,
        hairColor: hairColor ?? this.hairColor,
        skinColor: skinColor ?? this.skinColor,
        eyeColor: eyeColor ?? this.eyeColor,
        birthYear: birthYear ?? this.birthYear,
        gender: gender ?? this.gender,
        films: films ?? this.films,
        species: species ?? this.species,
        url: url ?? this.url,
        created: created ?? this.created,
        edited: edited ?? this.edited,
      );

  static People fromJson(Map<String, Object?> json) => People(
        id: json[PeopleFields.id] as int?,
        skinColor: json[PeopleFields.skinColor] as String,
        url: json[PeopleFields.url] as String,
        films: json[PeopleFields.films] == null
            ? null
            : List<String>.from(
                jsonDecode(json[PeopleFields.films] as String).map((x) => x)),
        eyeColor: json[PeopleFields.eyeColor] as String,
        height: json[PeopleFields.height] as String,
        mass: json[PeopleFields.mass] as String,
        edited: DateTime.parse(json[PeopleFields.edited] as String),
        birthYear: json[PeopleFields.birthYear] as String,
        gender: json[PeopleFields.gender] as String,
        name: json[PeopleFields.name] as String,
        species: json[PeopleFields.species] == null
            ? null
            : List<String>.from(
                jsonDecode(json[PeopleFields.species] as String).map((x) => x)),
        created: DateTime.parse(json[PeopleFields.created] as String),
        hairColor: json[PeopleFields.hairColor] as String,
      );

  Map<String, Object?> toJson() => {
        PeopleFields.id: id,
        PeopleFields.name: name,
        PeopleFields.height: height,
        PeopleFields.mass: mass,
        PeopleFields.hairColor: hairColor,
        PeopleFields.skinColor: skinColor,
        PeopleFields.eyeColor: eyeColor,
    PeopleFields.birthYear: birthYear,
    PeopleFields.gender: gender,
        PeopleFields.species: jsonEncode(species).toString(),
        PeopleFields.films: jsonEncode(films).toString(),
        PeopleFields.url: url,
        PeopleFields.created: created.toIso8601String(),
        PeopleFields.edited: edited.toIso8601String(),
      };
}
