// To parse this JSON data, do
//
//     final listPeopleModel = listPeopleModelFromJson(jsonString);

import 'dart:convert';

ListPeopleModel listPeopleModelFromJson(String str) => ListPeopleModel.fromJson(json.decode(str));

String listPeopleModelToJson(ListPeopleModel data) => json.encode(data.toJson());

class ListPeopleModel {
  ListPeopleModel({
    required this.count,
    required this.next,
    this.previous,
    required this.results,
  });

  int count;
  String next;
  dynamic previous;
  List<ResultPeople> results;

  factory ListPeopleModel.fromJson(Map<String, dynamic> json) => ListPeopleModel(
    count: json["count"],
    next: json["next"],
    previous: json["previous"],
    results: List<ResultPeople>.from(json["results"].map((x) => ResultPeople.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "count": count,
    "next": next,
    "previous": previous,
    "results": List<dynamic>.from(results.map((x) => x.toJson())),
  };
}

class ResultPeople {
  ResultPeople({
    required this.name,
    required this.height,
    required this.mass,
    required this.hairColor,
    required this.skinColor,
    required this.eyeColor,
    required this.birthYear,
    required this.gender,
    required this.homeworld,
    this.films,
    this.species,
    this.vehicles,
    this.starships,
    required this.created,
    required this.edited,
    required this.url,
  });

  String name;
  String height;
  String mass;
  String hairColor;
  String skinColor;
  String eyeColor;
  String birthYear;
  String gender;
  String homeworld;
  List<String>? films;
  List<String>? species;
  List<String>? vehicles;
  List<String>? starships;
  DateTime created;
  DateTime edited;
  String url;

  factory ResultPeople.fromJson(Map<String, dynamic> json) => ResultPeople(
    name: json["name"],
    height: json["height"],
    mass: json["mass"],
    hairColor: json["hair_color"],
    skinColor: json["skin_color"],
    eyeColor: json["eye_color"],
    birthYear: json["birth_year"],
    gender: json["gender"],
    homeworld: json["homeworld"],
    films: json["films"] == null ? null : List<String>.from(json["films"].map((x) => x)),
    species: json["species"] == null ? null :  List<String>.from(json["species"].map((x) => x)),
    vehicles: json["vehicles"] == null ? null :  List<String>.from(json["vehicles"].map((x) => x)),
    starships: json["starships"] == null ? null :  List<String>.from(json["starships"].map((x) => x)),
    created: DateTime.parse(json["created"]),
    edited: DateTime.parse(json["edited"]),
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "height": height,
    "mass": mass,
    "hair_color": hairColor,
    "skin_color": skinColor,
    "eye_color": eyeColor,
    "birth_year": birthYear,
    "gender": gender,
    "homeworld": homeworld,
    "films": films == null ? null :  List<dynamic>.from(films!.map((x) => x)),
    "species": species == null ? null : List<dynamic>.from(species!.map((x) => x)),
    "vehicles": vehicles == null ? null : List<dynamic>.from(vehicles!.map((x) => x)),
    "starships": starships == null ? null : List<dynamic>.from(starships!.map((x) => x)),
    "created": created.toIso8601String(),
    "edited": edited.toIso8601String(),
    "url": url,
  };
}
