import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/common/constants/api_constant.dart';

class ApiClient {
  late final Dio dio;
  late DioCacheManager dioCacheManager;

  ApiClient({required this.dio}) {
    dio.interceptors.add(InterceptorsWrapper(
        onRequest: (RequestOptions options, handler) =>
            requestInterceptor(options, handler),
        onError: onErrorInterceptor));
    dioCacheManager = DioCacheManager(CacheConfig());
    dio.interceptors.add(dioCacheManager.interceptor);
  }
  dynamic onErrorInterceptor(DioError e, ErrorInterceptorHandler h) async {
    if (e.response?.statusCode == 403) {
      try {
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        String? refreshToken = sharedPreferences.getString('refreshToken');
        await dio
            .post(ApiConstants.baseUrl + '/auth/refresh',
            data: jsonEncode({"refresh_token": refreshToken}))
            .then((value) async {
          if (value.statusCode == 200) {
            //get new tokens ...
            String token = value.data['results']['access_token'];
            String refreshToken = value.data['results']['refresh_token'];
            sharedPreferences.setString('refreshToken', refreshToken);
            sharedPreferences.setString('token', token);
            //set bearer
            e.requestOptions.headers["Authorization"] = "Bearer " + token;
            //create request with new access token
            final opts = Options(
                method: e.requestOptions.method,
                headers: e.requestOptions.headers);
            final cloneReq = await dio.request(e.requestOptions.path,
                options: opts,
                data: e.requestOptions.data,
                queryParameters: e.requestOptions.queryParameters);

            return h.resolve(cloneReq);
          }
          return h.next(e);
        });
        return dio;
      } catch (er) {
        return h.next(e);
      }
    } else {
      return h.next(e);
    }
  }

  dynamic requestInterceptor(
      RequestOptions options, RequestInterceptorHandler handler) async {
    if (options.headers.containsKey("requiresToken")) {
      options.headers.remove("requiresToken");
      SharedPreferences sharedPreferences =
      await SharedPreferences.getInstance();
      String? token = '';
      token = sharedPreferences.getString('token');
      options.headers.addAll({"Authorization": "Bearer $token"});
    }
    return handler.next(options);
  }

  dynamic get(String path) async {
    String url = ApiConstants.baseUrl + path;
    print(url);
    final response =
    await dio.get(url, options: Options(headers: {"requiresToken": true}));

    return jsonDecode(jsonEncode(response.data));
  }

  dynamic getWithoutBase(String path) async {
    String url =  path;
    print(url);
    final response =
    await dio.get(url, options: Options(headers: {"requiresToken": true}));

    return jsonDecode(jsonEncode(response.data));
  }
}