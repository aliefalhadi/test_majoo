import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:test_majoo/data/models/favorit_model.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/data/models/user_model.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  static Database? _db;

  Future<Database?> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "data_flutter.db");

    var ourDb = await openDatabase(path,version: 4, onCreate: _createDB);
    return ourDb;
  }

  FutureOr<void> _createDB(Database db, int version) async{
    const idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    const textType = 'TEXT NOT NULL';
    const boolType = 'BOOLEAN NOT NULL';
    const integerType = 'INTEGER NOT NULL';

    await db.execute('''
    CREATE TABLE users ( 
      ${UserFields.id} $idType, 
      ${UserFields.username} $textType,
      ${UserFields.email} $textType,
      ${UserFields.password} $textType,
      ${UserFields.time} $textType
      )
    ''');

    await db.execute('''
    CREATE TABLE peoples ( 
      ${PeopleFields.id} $idType, 
      ${PeopleFields.name} $textType,
      ${PeopleFields.height} $textType,
      ${PeopleFields.mass} $textType,
      ${PeopleFields.hairColor} $textType,
      ${PeopleFields.skinColor} $textType,
      ${PeopleFields.eyeColor} $textType,
      ${PeopleFields.birthYear} $textType,
      ${PeopleFields.gender} $textType,
      ${PeopleFields.films} $textType,
      ${PeopleFields.species} $textType,
      ${PeopleFields.url} $textType,
      ${PeopleFields.created} $textType,
      ${PeopleFields.edited} $textType
      )
    ''');

    await db.execute('''
    CREATE TABLE peoples_favorit ( 
      ${FavoritFields.idFavorit} $idType, 
      ${FavoritFields.idUser} $integerType, 
      ${FavoritFields.idPeople} $integerType
      )
    ''');
  }
}