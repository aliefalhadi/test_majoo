import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/data/core/dabatase_helper.dart';
import 'package:test_majoo/data/models/favorit_model.dart';
import 'package:test_majoo/data/models/people_model.dart';

abstract class FavoritLocalDataSource {
  Future<List<Favorit>> getListFavorit();
  Future<List<People>> getListFavoritPeople();
  Future<int> createFavorit(Favorit favorit);
  Future<bool> deleteFavorit(int idFavorit);
  Future<bool> deleteFavoritByPeople(int idPeople);
}

class FavoritLocalDataSourceImpl extends FavoritLocalDataSource{
  final DatabaseHelper _databaseHelper;

  FavoritLocalDataSourceImpl(this._databaseHelper);
  @override
  Future<List<Favorit>> getListFavorit() async{
    var dbClient = await _databaseHelper.db;

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');

    final result = await dbClient!.query('peoples_favorit',where: "${FavoritFields.idUser} = $idUser");
    print(result);
    return result.map((json) => Favorit.fromJson(json)).toList();
  }

  @override
  Future<int> createFavorit(Favorit favorit) async{
    var dbClient = await _databaseHelper.db;

    print(favorit.toJson());

    int index = await dbClient!.insert("peoples_favorit", favorit.toJson());
    return index;
  }

  @override
  Future<bool> deleteFavorit(int idFavorit) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');

    var dbClient = await _databaseHelper.db;
    await dbClient!.delete(
      "peoples_favorit",
      where: '${FavoritFields.idUser} = ? and ${FavoritFields.idPeople} = ?',
      whereArgs: [idUser,idFavorit],
    );
    return true;
  }

  @override
  Future<bool> deleteFavoritByPeople(int idPeople) async{
    var dbClient = await _databaseHelper.db;
    await dbClient!.delete(
      "peoples_favorit",
      where: '${FavoritFields.idPeople} = ?',
      whereArgs: [idPeople],
    );
    return true;
  }

  @override
  Future<List<People>> getListFavoritPeople() async{
    var dbClient = await _databaseHelper.db;

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');

    final result = await dbClient!.rawQuery('select * from peoples_favorit inner join peoples on peoples_favorit.id_people = peoples._id where peoples_favorit.id_user=$idUser');
    print(result);
    return result.map((json) => People.fromJson(json)).toList();
  }
}