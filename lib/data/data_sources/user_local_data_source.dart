
import 'package:test_majoo/data/core/dabatase_helper.dart';
import 'package:test_majoo/data/models/user_model.dart';

abstract class UserLocalDataSource {
  Future<bool> createUser(Map<String, dynamic> data);
  Future<User> getUser(Map<String, dynamic> data);
}

class UserLocalDataSourceImpl extends UserLocalDataSource{
  final DatabaseHelper _databaseHelper;

  UserLocalDataSourceImpl(this._databaseHelper);

  @override
  Future<bool> createUser(Map<String, dynamic> data) async{
    User user = User(
      username: data['username'],
      email: data['email'],
      password: data['password'],
      createdTime: DateTime.now(),
    );
    var dbClient = await _databaseHelper.db;
    await dbClient!.insert("users", user.toJson());
    return true;
  }

  @override
  Future<User> getUser(Map<String, dynamic> data) async{
    var dbClient = await _databaseHelper.db;
    final maps = await dbClient!.query(
      'users',
      columns: UserFields.values,
      where: '${UserFields.username} = ? and ${UserFields.password} = ?  ',
      whereArgs: [data['username'], data['password']],
    );

    if (maps.isNotEmpty) {
      return User.fromJson(maps.first);
    } else {
      throw Exception('user not found');
    }
  }
}