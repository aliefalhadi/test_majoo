import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/data/core/dabatase_helper.dart';
import 'package:test_majoo/data/models/people_model.dart';

abstract class PeopleLocalDataSource {
  Future<bool> checkDataPeopleLocal();
  Future<bool> createLocalPeople(People people);
  Future<bool> updateLocalPeople(People people);
  Future<People> detailLocalPeople(String idPeople);
  Future<bool> deletePeople(int idPeople);
  Future<bool> favoritLocalPeople(String idPeople, bool isFavorit);
  Future<List<People>> getListLocalPeople();
  Future<List<People>> getListFavoritPeople();
}

class PeopleLocalDataSourceImpl extends PeopleLocalDataSource{
  final DatabaseHelper _databaseHelper;

  PeopleLocalDataSourceImpl(this._databaseHelper);

  @override
  Future<bool> checkDataPeopleLocal() async{
    var dbClient = await _databaseHelper.db;

    final result = await dbClient!.query('peoples');


    if (result.isEmpty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> createLocalPeople(People people) async{

    var dbClient = await _databaseHelper.db;

    await dbClient!.insert("peoples", people.toJson());
    return true;
  }

  @override
  Future<List<People>> getListLocalPeople() async{
    var dbClient = await _databaseHelper.db;

    const orderBy = '${PeopleFields.created} DESC';

    final result = await dbClient!.query('peoples',orderBy: orderBy);

    return result.map((json) => People.fromJson(json)).toList();
  }

  @override
  Future<bool> updateLocalPeople(People people) async{
    var dbClient = await _databaseHelper.db;
    dbClient!.update(
      'peoples',
      people.toJson(),
      where: '${PeopleFields.id} = ?',
      whereArgs: [people.id],
    );
     return true;
  }

  @override
  Future<List<People>> getListFavoritPeople() async{
    var dbClient = await _databaseHelper.db;

    final result = await dbClient!.query(
        'peoples',
      where: '${PeopleFields.gender} = 1'
    );

    return result.map((json) => People.fromJson(json)).toList();
  }

  @override
  Future<bool> favoritLocalPeople(String idPeople, bool isFavorit) async{
    var dbClient = await _databaseHelper.db;

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? idUser = sharedPreferences.getString('id');

    if(isFavorit){
      await dbClient!.delete(
        'peoples_favorit',
        where: 'id_user = ? and id_people=?',
        whereArgs: [idUser, idPeople],
      );
    }else{
      await dbClient!.insert("peoples_favorit", {
        'id_user' : idUser!,
        'id_people' : idPeople,
      });
    }


    return true;
  }

  @override
  Future<People> detailLocalPeople(String idPeople) async{
    var dbClient = await _databaseHelper.db;

    int idPeople2 = int.parse(idPeople);
    final result = await dbClient!.query(
        'peoples',
      columns: PeopleFields.values,
      where: '${PeopleFields.id} = ?',
      whereArgs: [idPeople2]
    );



    if (result.isNotEmpty) {
      return People.fromJson(result.first);
    } else {
      throw Exception('ID not found');
    }
  }

  @override
  Future<bool> deletePeople(int idPeople) async{
    var dbClient = await _databaseHelper.db;
    await dbClient!.delete(
      "peoples",
      where: '${PeopleFields.id} = ?',
      whereArgs: [idPeople],
    );
    return true;
  }
}