import 'package:test_majoo/data/core/api_client.dart';
import 'package:test_majoo/data/models/detail_film_people_model.dart';
import 'package:test_majoo/data/models/detail_species_people_model.dart';
import 'package:test_majoo/data/models/list_people_model.dart';

abstract class PeopleRemoteDataSource{
  Future<ListPeopleModel> getListPeople();
  Future<DetailFilmPeopleModel> getDetailFilmPeople(String url);
  Future<DetailSpeciesPeopleModel> getDetailSpeciesPeople(String url);
}

class PeopleRemoteDataSourceImpl extends PeopleRemoteDataSource{
  final ApiClient _client;

  PeopleRemoteDataSourceImpl(this._client);

  @override
  Future<ListPeopleModel> getListPeople() async{
    final response = await _client.get('/people');
    return ListPeopleModel.fromJson(response);
  }

  @override
  Future<DetailFilmPeopleModel> getDetailFilmPeople(String url) async{
    final response = await _client.getWithoutBase(url);
    return DetailFilmPeopleModel.fromJson(response);
  }

  @override
  Future<DetailSpeciesPeopleModel> getDetailSpeciesPeople(String url) async{
    final response = await _client.getWithoutBase(url);
    return DetailSpeciesPeopleModel.fromJson(response);
  }
}