import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/common/constants/route_constants.dart';
import 'package:test_majoo/data/models/user_model.dart';

abstract class AuthenticationLocalDataSource {
  Future<bool> saveSession(User user);
  Future<String> checkLoginSession();
  Future<User> getUserLogin();
  Future<String> getIdUserLogin();
  Future<void> deleteSession();
}


class AuthenticationLocalDataSourceImpl extends AuthenticationLocalDataSource {
  @override
  Future<bool> saveSession(User user) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    sharedPreferences.setString('id', user.id.toString());
    sharedPreferences.setString('user', jsonEncode(user.toJson()));
    sharedPreferences.setBool('isLogin', true);

    return true;
  }

  @override
  Future<String> checkLoginSession() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.containsKey('isLogin')){
      return RouteList.home;
    }
    return RouteList.login;
  }

  @override
  Future<User> getUserLogin() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? user = sharedPreferences.getString('user');
    return User.fromJson(jsonDecode(user!));
  }

  @override
  Future<void> deleteSession() async{
    print('delete session - local');

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.clear();
  }

  @override
  Future<String> getIdUserLogin() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String? id = sharedPreferences.getString('id');
    return id!;
  }
}