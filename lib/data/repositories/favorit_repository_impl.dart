import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/data_sources/favorit_local_data_source.dart';
import 'package:test_majoo/data/models/favorit_model.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/favorit_repository.dart';

class FavoritRepositoryImpl extends FavoritRepository {
  final FavoritLocalDataSource _favoritLocalDataSource;

  FavoritRepositoryImpl(this._favoritLocalDataSource);
  @override
  Future<Either<AppError, List<Favorit>>> getListFavorit() async{
    try {
      final listFavorit = await _favoritLocalDataSource.getListFavorit();
      return Right(listFavorit);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, int>> createFavorit(Favorit favorit) async{
    try {
     int res = await _favoritLocalDataSource.createFavorit(favorit);
     print(res);
      return Right(res);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, bool>> deleteFavorit(int idFavorit) async{
    try {
      print(idFavorit);
      bool res = await _favoritLocalDataSource.deleteFavorit(idFavorit);
      print(res);
      return const Right(true);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, List<People>>> getListFavoritPeople() async{
    try {
      final listFavorit = await _favoritLocalDataSource.getListFavoritPeople();
      return Right(listFavorit);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }
}