import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/data_sources/auth_local_data_source.dart';
import 'package:test_majoo/data/data_sources/user_local_data_source.dart';
import 'package:test_majoo/data/models/user_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/user_repository.dart';

class UserRepositoryImpl extends UserRepository {
  final UserLocalDataSource userLocalDataSource;
  final AuthenticationLocalDataSource authenticationLocalDataSource;

  UserRepositoryImpl(this.userLocalDataSource, this.authenticationLocalDataSource);
  @override
  Future<Either<AppError, bool>> register(Map<String, dynamic> params) async {
    try {
      await userLocalDataSource.createUser(params);
      return const Right(true);
    } on Exception {
      return const Left(AppError(AppErrorType.api));
    }
  }

  @override
  Future<Either<AppError, bool>> login(Map<String, dynamic> params) async{
    try {
      User user = await userLocalDataSource.getUser(params);
      print(user.toString());
      await authenticationLocalDataSource.saveSession(user);
      return const Right(true);
    } on Exception {
      return const Left(AppError(AppErrorType.api));
    }
  }

  @override
  Future<String> checkUserLogin() async{
    String routeInitial =
        await authenticationLocalDataSource.checkLoginSession();
    return routeInitial;
  }

  @override
  Future<User> getUserLogin() async{
    return await authenticationLocalDataSource.getUserLogin();
  }

  @override
  Future<Either<AppError, void>> logoutUser() async{
    await Future.wait([authenticationLocalDataSource.deleteSession()]);
    return const Right(Unit);
  }
}
