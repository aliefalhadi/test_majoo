import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:test_majoo/data/data_sources/favorit_local_data_source.dart';
import 'package:test_majoo/data/data_sources/people_local_data_source.dart';
import 'package:test_majoo/data/data_sources/people_remote_data_source.dart';
import 'package:test_majoo/data/models/detail_film_people_model.dart';
import 'package:test_majoo/data/models/detail_species_people_model.dart';
import 'package:test_majoo/data/models/list_people_model.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';

class PeopleRepositoryImpl extends PeopleRepository {
  final PeopleRemoteDataSource _peopleRemoteDataSource;
  final PeopleLocalDataSource _peopleLocalDataSource;
  final FavoritLocalDataSource _favoritLocalDataSource;

  PeopleRepositoryImpl(
      this._peopleRemoteDataSource, this._peopleLocalDataSource, this._favoritLocalDataSource);
  @override
  Future<Either<AppError, List<People>>> getListPeople() async {
    try {
      bool isEmptyLocal = await _peopleLocalDataSource.checkDataPeopleLocal();
      // ListPeopleModel listPeopleModel = await _peopleRemoteDataSource.getListPeople();
      List<People> listPeople = [];
      if (isEmptyLocal) {
        ListPeopleModel listPeopleModel =
            await _peopleRemoteDataSource.getListPeople();
        for (var data in listPeopleModel.results) {
          People people = People(
              name: data.name,
              height: data.height,
              mass: data.mass,
              hairColor: data.hairColor,
              skinColor: data.skinColor,
              eyeColor: data.eyeColor,
              birthYear: data.birthYear,
              gender: data.gender,
              films: data.films,
              species: data.species,
              url: data.url,
              created: data.created,
              edited: data.edited);

          await _peopleLocalDataSource.createLocalPeople(people);
        }
      }
      listPeople = await _peopleLocalDataSource.getListLocalPeople();

      return Right(listPeople);
    } on SocketException {
      return const Left(AppError(AppErrorType.network));
    } on DioError catch (e) {
      if (e.type == DioErrorType.connectTimeout ||
          e.type == DioErrorType.receiveTimeout ||
          e.type == DioErrorType.sendTimeout) {
        return const Left(AppError(AppErrorType.network));
      }
      return Left(AppError(AppErrorType.api,
          message: e.response?.data['message'] ?? 'ada kesalahan'));
    } on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, bool>> updatePeople(People people) async{
    try{
      await _peopleLocalDataSource.updateLocalPeople(people);
      return const Right(true);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, List<People>>> getListFavoritPeople() async{
    try{
     final data = await _peopleLocalDataSource.getListFavoritPeople();

      return Right(data);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, bool>> favoritPeople(String idPeople, bool isFavorit) async{
    try{
      await _peopleLocalDataSource.favoritLocalPeople(idPeople, isFavorit);

      return const Right(true);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, People>> getDetailPeople(String idPeople) async{
    try{
      final data = await _peopleLocalDataSource.detailLocalPeople(idPeople);

      return Right(data);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, bool>> createPeople(People people) async{
    try {
      bool res = await _peopleLocalDataSource.createLocalPeople(people);
      print(res);
      return const Right(true);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, bool>> deletePeople(int idPeople) async{
    try {
      bool res = await _peopleLocalDataSource.deletePeople(idPeople);
      bool resFavorit = await _favoritLocalDataSource.deleteFavoritByPeople(idPeople);
      print(resFavorit);
      print(res);
      return const Right(true);
    }on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, DetailFilmPeopleModel>> getListFilmPeople(String url) async{
    try {
      DetailFilmPeopleModel detailFilmPeopleModel =
      await _peopleRemoteDataSource.getDetailFilmPeople(url);
      return  Right(detailFilmPeopleModel);
    } on SocketException {
      return const Left(AppError(AppErrorType.network));
    } on DioError catch (e) {
      if (e.type == DioErrorType.connectTimeout ||
          e.type == DioErrorType.receiveTimeout ||
          e.type == DioErrorType.sendTimeout) {
        return const Left(AppError(AppErrorType.network));
      }
      return Left(AppError(AppErrorType.api,
          message: e.response?.data['message'] ?? 'ada kesalahan'));
    } on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }

  @override
  Future<Either<AppError, DetailSpeciesPeopleModel>> getDetailSpeciesPeople(String url) async{
    try {
      DetailSpeciesPeopleModel detailSpeciesPeopleModel =
      await _peopleRemoteDataSource.getDetailSpeciesPeople(url);
      return  Right(detailSpeciesPeopleModel);
    } on SocketException {
      return const Left(AppError(AppErrorType.network));
    } on DioError catch (e) {
      if (e.type == DioErrorType.connectTimeout ||
          e.type == DioErrorType.receiveTimeout ||
          e.type == DioErrorType.sendTimeout) {
        return const Left(AppError(AppErrorType.network));
      }
      return Left(AppError(AppErrorType.api,
          message: e.response?.data['message'] ?? 'ada kesalahan'));
    } on Exception {
      return const Left(AppError(AppErrorType.api, message: 'ada kesalahan'));
    }
  }
}
