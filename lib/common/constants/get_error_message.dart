
import 'package:test_majoo/domain/entities/app_error.dart';

String getErrorMessage(AppErrorType appErrorType) {
  switch (appErrorType) {
    case AppErrorType.network:
      return 'tidak ada jaringan';
    case AppErrorType.api:
    case AppErrorType.database:
      return 'telah terjadi kesalahan';
    case AppErrorType.sessionDenied:
      return 'session tidak ada';
    default:
      return 'telah terjadi kesalahan';
  }
}