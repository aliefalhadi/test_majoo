import 'package:dartz/dartz.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/user_repository.dart';

class LogoutUserCase {
  final UserRepository _userRepository;

  LogoutUserCase(this._userRepository);

  @override
  Future<Either<AppError, void>> call() async =>
      _userRepository.logoutUser();
}