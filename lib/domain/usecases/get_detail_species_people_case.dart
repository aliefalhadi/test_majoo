import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/detail_species_people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';

class GetDetailSpeciesPeopleCase{
  final PeopleRepository peopleRepository;

  GetDetailSpeciesPeopleCase(this.peopleRepository);

  Future<Either<AppError, DetailSpeciesPeopleModel>> call(String url) async => peopleRepository.getDetailSpeciesPeople(url);
}