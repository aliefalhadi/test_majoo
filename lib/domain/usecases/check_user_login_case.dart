import 'package:test_majoo/domain/repositories/user_repository.dart';

class CheckUserLoginCase{
  final UserRepository userRepository;

  CheckUserLoginCase(this.userRepository);

  Future call() async => userRepository.checkUserLogin();
}