import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';

class GetListPeopleCase{
  final PeopleRepository peopleRepository;

  GetListPeopleCase(this.peopleRepository);

  Future<Either<AppError, List<People>>> call() async => peopleRepository.getListPeople();
}