import 'package:dartz/dartz.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';

class FavoritPeopleCase{
  final PeopleRepository peopleRepository;

  FavoritPeopleCase(this.peopleRepository);

  Future<Either<AppError, bool>> call(String idPeople, bool isFavorit) async => peopleRepository.favoritPeople(idPeople, isFavorit);
}