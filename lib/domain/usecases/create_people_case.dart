import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/favorit_model.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/favorit_repository.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';

class CreatePeopleCase{
  final PeopleRepository peopleRepository;

  CreatePeopleCase(this.peopleRepository);

  Future<Either<AppError, bool>> call(People params) async => peopleRepository.createPeople(params);
}