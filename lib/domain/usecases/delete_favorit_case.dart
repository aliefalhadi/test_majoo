import 'package:dartz/dartz.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/favorit_repository.dart';

class DeleteFavoritCase{
  final FavoritRepository favoritRepository;

  DeleteFavoritCase(this.favoritRepository);

  Future<Either<AppError, bool>> call(int params) async => favoritRepository.deleteFavorit(params);
}