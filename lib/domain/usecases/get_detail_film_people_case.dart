import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/detail_film_people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';

class GetDetailFilmPeopleCase{
  final PeopleRepository peopleRepository;

  GetDetailFilmPeopleCase(this.peopleRepository);

  Future<Either<AppError, DetailFilmPeopleModel>> call(String url) async => peopleRepository.getListFilmPeople(url);
}