import 'package:dartz/dartz.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/entities/register_entity.dart';
import 'package:test_majoo/domain/repositories/user_repository.dart';

class RegisterUserCase{
  final UserRepository _userRepository;

  RegisterUserCase(this._userRepository);

  Future<Either<AppError, bool>> call(RegisterEntity params) async => _userRepository.register(params.toJson());
}