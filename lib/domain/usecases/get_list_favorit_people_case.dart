import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/favorit_repository.dart';

class GetListFavoritPeopleCase{
  final FavoritRepository favoritRepository;

  GetListFavoritPeopleCase(this.favoritRepository);

  Future<Either<AppError, List<People>>> call() async => favoritRepository.getListFavoritPeople();
}