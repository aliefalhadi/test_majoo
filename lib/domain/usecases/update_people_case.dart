import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';

class UpdatePeopleCase{
  final PeopleRepository peopleRepository;

  UpdatePeopleCase(this.peopleRepository);

  Future<Either<AppError, bool>> call(People params) async => peopleRepository.updatePeople(params);
}