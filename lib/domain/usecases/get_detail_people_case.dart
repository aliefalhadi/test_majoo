import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';

class GetDetailPeopleCase{
  final PeopleRepository peopleRepository;

  GetDetailPeopleCase(this.peopleRepository);

  Future<Either<AppError,People>> call(String params) async => peopleRepository.getDetailPeople(params);
}