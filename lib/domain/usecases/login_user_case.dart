import 'package:dartz/dartz.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/entities/login_entity.dart';
import 'package:test_majoo/domain/repositories/user_repository.dart';

class LoginUserCase{
  final UserRepository _userRepository;

  LoginUserCase(this._userRepository);

  Future<Either<AppError, bool>> call(LoginEntity params) async => _userRepository.login(params.toJson());
}