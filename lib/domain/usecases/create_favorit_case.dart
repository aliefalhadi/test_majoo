import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/favorit_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/favorit_repository.dart';

class CreateFavoritCase{
  final FavoritRepository favoritRepository;

  CreateFavoritCase(this.favoritRepository);

  Future<Either<AppError, int>> call(Favorit params) async => favoritRepository.createFavorit(params);
}