import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/favorit_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/favorit_repository.dart';

class GetListFavoritCase{
  final FavoritRepository favoritRepository;

  GetListFavoritCase(this.favoritRepository);

  Future<Either<AppError, List<Favorit>>> call() async => favoritRepository.getListFavorit();
}