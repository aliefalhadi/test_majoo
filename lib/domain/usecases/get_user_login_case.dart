import 'package:test_majoo/data/models/user_model.dart';
import 'package:test_majoo/domain/repositories/user_repository.dart';

class GetUserLoginCase{
  final UserRepository userRepository;

  GetUserLoginCase(this.userRepository);

  Future<User> call() => userRepository.getUserLogin();
}