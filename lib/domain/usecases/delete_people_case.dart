import 'package:dartz/dartz.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/repositories/people_repository.dart';

class DeletePeopleCase{
  final PeopleRepository peopleRepository;

  DeletePeopleCase(this.peopleRepository);

  Future<Either<AppError, bool>> call(int params) async => peopleRepository.deletePeople(params);
}