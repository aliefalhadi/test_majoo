import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/detail_film_people_model.dart';
import 'package:test_majoo/data/models/detail_species_people_model.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';

abstract class PeopleRepository {
  Future<Either<AppError, List<People>>> getListPeople();
  Future<Either<AppError, DetailFilmPeopleModel>> getListFilmPeople(String url);
  Future<Either<AppError,DetailSpeciesPeopleModel>> getDetailSpeciesPeople(String url);
  Future<Either<AppError, bool>> createPeople(People people);
  Future<Either<AppError, People>> getDetailPeople(String idPeople);
  Future<Either<AppError, List<People>>> getListFavoritPeople();
  Future<Either<AppError, bool>> updatePeople(People people);
  Future<Either<AppError, bool>> deletePeople(int idPeople);
  Future<Either<AppError, bool>> favoritPeople(String idPeople, bool isFavorit);
}