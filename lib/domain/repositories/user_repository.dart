import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/user_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';

abstract class UserRepository {
  Future <Either<AppError,bool>> register(Map<String,dynamic> params);
  Future <Either<AppError,bool>> login(Map<String,dynamic> params);
  Future<String> checkUserLogin();
  Future<User> getUserLogin();
  Future<Either<AppError, void>> logoutUser();
}