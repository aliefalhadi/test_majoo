import 'package:dartz/dartz.dart';
import 'package:test_majoo/data/models/favorit_model.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/app_error.dart';

abstract class FavoritRepository {
  Future<Either<AppError, List<Favorit>>> getListFavorit();
  Future<Either<AppError, List<People>>> getListFavoritPeople();
  Future<Either<AppError, int>> createFavorit(Favorit favorit);
  Future<Either<AppError, bool>> deleteFavorit(int idFavorit);
}