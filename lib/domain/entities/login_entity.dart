class LoginEntity{
  late  String username;
  late  String password;

  LoginEntity({required this.username, required this.password});

  Map<String, dynamic> toJson() => {
    'username': username,
    'password': password,
  };
}