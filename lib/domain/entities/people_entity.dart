class PeopleEntity{
  late  String name;
  late  String height;
  late  String mass;
  late  String hairColor;
  late  String skinColor;
  late  String eyeColor;
  late  String birthYear;
  late  String gender;

  PeopleEntity({required this.name, required this.height, required this.mass, required this.hairColor, required this.skinColor, required this.eyeColor,required this.birthYear, required this.gender});
}