import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:pedantic/pedantic.dart';
import 'package:test_majoo/common/constants/route_constants.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/domain/usecases/check_user_login_case.dart';
import 'package:test_majoo/presentation/blocs/loading/loading_bloc.dart';
import 'package:test_majoo/presentation/routes.dart';
import 'package:test_majoo/presentation/views/loading/loading_screen.dart';
import 'di/get_it.dart' as get_it;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  unawaited(
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]));

  await get_it.init();

  CheckUserLoginCase checkUserLogin = get_it.getItInstance<CheckUserLoginCase>();
  final String routeInitial = await checkUserLogin();
  print(routeInitial);


  runApp(MyApp(routeInitial: routeInitial,));
}

class MyApp extends StatefulWidget {
  final String routeInitial;
  const MyApp({Key? key, required this.routeInitial}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late LoadingBloc _loadingBloc;

  @override
  void initState() {
    super.initState();
    _loadingBloc = getItInstance<LoadingBloc>();
  }

  @override
  void dispose() {
    _loadingBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoadingBloc>.value(
      value: _loadingBloc,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Test Majoo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        builder: EasyLoading.init(builder:(context, child){
          return LoadingScreen(
            screen: child!,
          );
        }),
        initialRoute: widget.routeInitial,
        onGenerateRoute: AppRouter().onGenerateRoute,
      ),
    );
  }
}
