import 'package:flutter/material.dart';
import 'package:test_majoo/presentation/views/dashboard/dashboard_view.dart';
import 'package:test_majoo/presentation/views/favorit/favorit_view.dart';
import 'package:test_majoo/presentation/views/profile/profile_view.dart';

class TabbedHomeWidget extends StatefulWidget {
  const TabbedHomeWidget({Key? key}) : super(key: key);

  @override
  _TabbedHomeWidgetState createState() => _TabbedHomeWidgetState();
}

class _TabbedHomeWidgetState extends State<TabbedHomeWidget> with SingleTickerProviderStateMixin {
  late int _currentIndex;
  late TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentIndex = 0;
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        onTap: (index){
          setState(() {
            _currentIndex = index;
            _tabController.index = index;
          });
        },
        currentIndex: _currentIndex,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Beranda"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: "Favorit"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Profile"
          ),
        ],
      ),
      body: TabBarView(
        controller: _tabController,
        physics: const NeverScrollableScrollPhysics(),
        children: const [
          DashboardView(),
          FavoritView(),
          ProfileView(),
        ],
      ),
    );
  }
}
