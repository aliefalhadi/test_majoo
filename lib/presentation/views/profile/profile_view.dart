import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/common/constants/route_constants.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/auth/auth_bloc.dart';
import 'package:test_majoo/presentation/themes/themes.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  late AuthBloc authBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    authBloc = getItInstance<AuthBloc>();
    authBloc.add(LoadingAuth());
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    authBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: authBloc,
      child: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if(state is AuthLogoutSuccess){
            Navigator.pushNamedAndRemoveUntil(context, RouteList.login, (route) => false);
          }
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Profile"),
          ),
          body: BlocBuilder<AuthBloc, AuthState>(
            builder: (context, state) {
              if (state is AuthLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is AuthLoaded) {
                return ListView(
                  children: [
                    Card(
                      color: Colors.green,
                      margin: EdgeInsets.zero,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Stack(
                              children: const [
                                CircleAvatar(
                                  radius: 65,
                                  backgroundColor: Colors.yellow,
                                  child: CircleAvatar(
                                    radius: 60,
                                    backgroundColor: Colors.white,
                                    backgroundImage: AssetImage(
                                      'assets/images/profile.png',
                                    ),
                                  ),
                                ),
                                Positioned(
                                  right: 0,
                                  bottom: 0,
                                  child: CircleAvatar(
                                    backgroundColor: Colors.yellow,
                                    child: Icon(
                                      Icons.edit, color: Colors.white,),
                                  ),
                                )
                              ],
                            ),
                            hSpace(16),
                            Text(
                              state.user.username,
                              style: whiteTextStyle.copyWith(
                                  fontSize: 18, fontWeight: bold),
                            ),
                            Text(
                              state.user.email,
                              style: whiteTextStyle,
                            ),

                          ],
                        ),
                      ),
                    ),
                    vSpace(24),
                    OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          backgroundColor: Colors.white,
                          padding: const EdgeInsets.all(16),
                          side: const BorderSide(color: Colors.red)
                      ),
                      child: Text("Keluar", style: redTextStyle,),
                      onPressed: () {
                        authBloc.add(LogoutAuth());
                      },
                    )
                  ],
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }
}
