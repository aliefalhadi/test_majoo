import 'package:flutter/material.dart';
import 'package:test_majoo/presentation/themes/themes.dart';
import 'package:test_majoo/presentation/views/register/register_form.dart';

class RegisterView extends StatelessWidget {
  const RegisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: ListView(
          children: [
            Text("Daftar Akun", style: blackTextStyle.copyWith(
                fontSize: 24, fontWeight: bold
            ),),
            vSpace(8),
            Text("Silahkan lengkapi data dibawah ini, ya", style: blackTextStyle.copyWith(
                fontWeight: light
            ),),
            vSpace(16),
             const RegisterForm(),
          ],
        ),
      ),
    );
  }
}
