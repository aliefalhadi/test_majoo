import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:test_majoo/common/constants/route_constants.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/register/register_bloc.dart';
import 'package:test_majoo/presentation/themes/themes.dart';
import 'package:test_majoo/presentation/widgets/label_field_widget.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  late RegisterBloc registerBloc;
  GlobalKey<FormState> globalKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    registerBloc = getItInstance<RegisterBloc>();
  }

  @override
  void dispose() {
    super.dispose();
    registerBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RegisterBloc>.value(
      value: registerBloc,
      child: BlocConsumer<RegisterBloc, RegisterState>(
        listener: (context, state) {
          if(state is RegisterError){
            EasyLoading.showInfo("telah terjadi kesalahan, silahkan coba lagi");
          }else if(state is RegisterSuccess){
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (context) {
                return WillPopScope(
                  onWillPop: () async => false,
                  child: AlertDialog(
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const Text("Registrasi pengguna berhasil, silahkan login dengan username dan password yang telah didaftarkan"),
                        const SizedBox(height: 8,),
                        SizedBox(
                          width: double.infinity,
                          child: ElevatedButton(
                            onPressed: (){
                              Navigator.pushNamedAndRemoveUntil(context, RouteList.login, (route) => false);
                            },
                            child: const Text("Login", style: TextStyle(color: Colors.white),),
                          ),
                        )
                      ],
                    ),

                  ),
                );
              });
          }
        },
        builder: (context, state) {
          return Form(
            key: globalKey,
            child: Column(
              children: [
                LabelFieldWidget(
                  label: "Username",
                  hintText: "Masukkan username",
                  onChange: (value) {
                    registerBloc.registerEntity.username = value;
                  },
                ),
                LabelFieldWidget(
                  label: "Email",
                  hintText: "Masukkan email",
                  typeInput: TextInputType.emailAddress,
                  onChange: (value) {
                    registerBloc.registerEntity.email = value;
                  },
                ),
                LabelFieldWidget(
                  isPasswordField: true,
                  label: "Password",
                  hintText: "Masukkan password",
                  onChange: (value) {
                    registerBloc.registerEntity.password = value;
                  },
                ),
                LabelFieldWidget(
                  isPasswordField: true,
                  label: "Ulangi Password",
                  hintText: "Ulangi masukkan password",
                  validator: (value){
                    if(value == ''){
                      return 'inputan tidak boleh kosong';
                    }else if(registerBloc.registerEntity.password != value){
                      return 'Password tidak sama';
                    }
                    return null;
                  },
                ),
                vSpace(16),
                FractionallySizedBox(
                  widthFactor: 1,
                  child: ElevatedButton(
                    child: Text(
                      "Daftar",
                      style: whiteTextStyle,
                    ),
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      if(globalKey.currentState!.validate()){
                        registerBloc.add(InitiateRegisterEvent(registerBloc.registerEntity));
                      }
                    },
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
