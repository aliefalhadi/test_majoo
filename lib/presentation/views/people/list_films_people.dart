import 'package:flutter/material.dart';
import 'package:test_majoo/common/constants/route_constants.dart';

class ListFilmsPeople extends StatelessWidget {
  final List<String> films;
  const ListFilmsPeople({Key? key, required this.films}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Lists Film People"),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: ListView(
          children: List.generate(films.length, (index){
            return Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.grey.withOpacity(0.2))
                )
              ),
              child: ListTile(
                onTap: (){
                  Navigator.pushNamed(context, RouteList.detailFilmsPeople, arguments: films[index]);
                },
                contentPadding: EdgeInsets.zero,
                leading: const Icon(Icons.drive_file_move),
                title: Text(films[index]),
              ),
            );
          }),
        ),
      ),
    );
  }
}
