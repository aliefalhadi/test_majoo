import 'package:flutter/material.dart';
import 'package:test_majoo/common/constants/route_constants.dart';

class ListSpeciesPeople extends StatelessWidget {
  final List<String> species;
  const ListSpeciesPeople({Key? key, required this.species}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Lists Species People"),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: ListView(
          children: List.generate(species.length, (index){
            return Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.grey.withOpacity(0.2))
                )
              ),
              child: ListTile(
                onTap: (){
                  Navigator.pushNamed(context, RouteList.detailSpeciesPeople, arguments: species[index]);
                },
                contentPadding: EdgeInsets.zero,
                leading: const Icon(Icons.drive_file_move),
                title: Text(species[index]),
              ),
            );
          }),
        ),
      ),
    );
  }
}
