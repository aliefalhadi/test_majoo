import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/detail-people/detail_people_bloc.dart';

class DetailFilmPeopleView extends StatefulWidget {
  final String url;
  const DetailFilmPeopleView({Key? key,required this.url}) : super(key: key);

  @override
  _DetailFilmPeopleViewState createState() => _DetailFilmPeopleViewState();
}

class _DetailFilmPeopleViewState extends State<DetailFilmPeopleView> {
  late DetailPeopleBloc detailPeopleBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    detailPeopleBloc = getItInstance<DetailPeopleBloc>();
    detailPeopleBloc.add(DetailFilmPeopleEvent(widget.url));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    detailPeopleBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: detailPeopleBloc,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Detail Film"),
        ),
        body: BlocBuilder<DetailPeopleBloc, DetailPeopleState>(
          builder: (context, state) {
            if(state is DetailPeopleLoading){
              return const Center(child: CircularProgressIndicator());
            }else if(state is DetailFilmPeopleSuccess){
              return Container(
                padding: const EdgeInsets.all(16),
                child: ListView(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Title"),
                        subtitle: Text(state.detailFilmPeopleModel.title),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Opening  Crawl"),
                        subtitle: Text(state.detailFilmPeopleModel.openingCrawl),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Director"),
                        subtitle: Text(state.detailFilmPeopleModel.director),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Producer"),
                        subtitle: Text(state.detailFilmPeopleModel.producer),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Release date"),
                        subtitle: Text(DateFormat('dd MMMM yyyy').format(state.detailFilmPeopleModel.releaseDate)),
                      ),
                    ),
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
