import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/people/people_bloc.dart';
import 'package:test_majoo/presentation/themes/themes.dart';
import 'package:test_majoo/presentation/widgets/label_field_widget.dart';

class CreatePeopleView extends StatefulWidget {
  const CreatePeopleView({Key? key}) : super(key: key);

  @override
  State<CreatePeopleView> createState() => _CreatePeopleViewState();
}

class _CreatePeopleViewState extends State<CreatePeopleView> {
  late final PeopleBloc peopleBloc;
  GlobalKey<FormState> globalKey = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    peopleBloc = getItInstance<PeopleBloc>();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    peopleBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PeopleBloc>.value(
      value: peopleBloc,
      child: BlocListener<PeopleBloc, PeopleState>(
        listener: (context, state) {
          if(state is PeopleSuccess){
            Navigator.pop(context, 'berhasil');
            EasyLoading.showSuccess("Berhasil menambahkan data");
          }
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Create People"),
          ),
          body: Container(
            padding: const EdgeInsets.all(16),
            child: Form(
              key: globalKey,
              child: ListView(
                children: [
                  LabelFieldWidget(
                    label: "Name",
                    hintText: "Masukkan name",
                    onChange: (value) {
                      peopleBloc.peopleEntity.name = value;
                    },
                  ),
                  LabelFieldWidget(
                    label: "Height",
                    hintText: "Masukkan height (ex:123)",
                    onChange: (value) {
                      peopleBloc.peopleEntity.height = value;
                    },
                  ),
                  LabelFieldWidget(
                    label: "Mass",
                    hintText: "Masukkan mass (ex:192)",
                    onChange: (value) {
                      peopleBloc.peopleEntity.mass = value;
                    },
                  ),
                  LabelFieldWidget(
                    label: "Hair Color",
                    hintText: "Masukkan hair color",
                    onChange: (value) {
                      peopleBloc.peopleEntity.hairColor = value;
                    },
                  ),
                  LabelFieldWidget(
                    label: "Skin Color",
                    hintText: "Masukkan skin color",
                    onChange: (value) {
                      peopleBloc.peopleEntity.skinColor = value;
                    },
                  ),
                  LabelFieldWidget(
                    label: "Eye Color",
                    hintText: "Masukkan eye color",
                    onChange: (value) {
                      peopleBloc.peopleEntity.eyeColor = value;
                    },
                  ),
                  LabelFieldWidget(
                    label: "Birth Year",
                    hintText: "Masukkan birth year",
                    onChange: (value) {
                      peopleBloc.peopleEntity.birthYear = value;
                    },
                  ),
                  LabelFieldWidget(
                    label: "Gender",
                    hintText: "Masukkan gender (ex:male/female)",
                    onChange: (value) {
                      peopleBloc.peopleEntity.gender = value;
                    },
                  ),
                  vSpace(16),
                  FractionallySizedBox(
                    widthFactor: 1,
                    child: ElevatedButton(
                      child: Text("SIMPAN", style: whiteTextStyle,),
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        if (globalKey.currentState!.validate()) {
                          peopleBloc.add(CreatePeopleEvent());
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
