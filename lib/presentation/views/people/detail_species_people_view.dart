import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/detail-people/detail_people_bloc.dart';

class DetailSpeciesPeopleView extends StatefulWidget {
  final String url;
  const DetailSpeciesPeopleView({Key? key,required this.url}) : super(key: key);

  @override
  _DetailSpeciesPeopleViewState createState() => _DetailSpeciesPeopleViewState();
}

class _DetailSpeciesPeopleViewState extends State<DetailSpeciesPeopleView> {
  late DetailPeopleBloc detailPeopleBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    detailPeopleBloc = getItInstance<DetailPeopleBloc>();
    detailPeopleBloc.add(DetailSpeciesPeopleEvent(widget.url));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    detailPeopleBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: detailPeopleBloc,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Detail Species"),
        ),
        body: BlocBuilder<DetailPeopleBloc, DetailPeopleState>(
          builder: (context, state) {
            if(state is DetailPeopleLoading){
              return const Center(child: CircularProgressIndicator());
            }else if(state is DetailSpeciesPeopleSuccess){
              return Container(
                padding: const EdgeInsets.all(16),
                child: ListView(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Name"),
                        subtitle: Text(state.detailSpeciesPeopleModel.name),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Classification"),
                        subtitle: Text(state.detailSpeciesPeopleModel.classification),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Designation"),
                        subtitle: Text(state.detailSpeciesPeopleModel.designation),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Average height"),
                        subtitle: Text(state.detailSpeciesPeopleModel.averageHeight),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Skin colors"),
                        subtitle: Text(state.detailSpeciesPeopleModel.skinColors),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Hair colors"),
                        subtitle: Text(state.detailSpeciesPeopleModel.hairColors),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Eye colors"),
                        subtitle: Text(state.detailSpeciesPeopleModel.eyeColors),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Average Lifespan"),
                        subtitle: Text(state.detailSpeciesPeopleModel.averageLifespan),
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2)))),
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: const Text("Language"),
                        subtitle: Text(state.detailSpeciesPeopleModel.language),
                      ),
                    ),
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
