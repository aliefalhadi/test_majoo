import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:intl/intl.dart';
import 'package:test_majoo/common/constants/route_constants.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/people/people_bloc.dart';
import 'package:test_majoo/presentation/widgets/custom_dialog.dart';

class DetailPeopleView extends StatefulWidget {
  final int idPeople;

  const DetailPeopleView({Key? key, required this.idPeople}) : super(key: key);

  @override
  State<DetailPeopleView> createState() => _DetailPeopleViewState();
}

class _DetailPeopleViewState extends State<DetailPeopleView> {
  late PeopleBloc peopleBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    peopleBloc = getItInstance<PeopleBloc>();
    peopleBloc.add(DetailPeopleEvent(widget.idPeople));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    peopleBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PeopleBloc>.value(
      value: peopleBloc,
      child: BlocListener<PeopleBloc, PeopleState>(
        listener: (context, state) {
          if(state is PeopleDeleted){
            Navigator.pushNamedAndRemoveUntil(context, RouteList.home, (route) => false);
            EasyLoading.showSuccess("berhasil hapus data");
          }
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Detail People"),
            actions: [
              PopupMenuButton(
                onSelected: (value) async{
                  if (value == 1) {
                    var data = await Navigator.pushNamed(context, RouteList.updatePeople, arguments: widget.idPeople);
                    if(data != null){
                      peopleBloc.add(DetailPeopleEvent(widget.idPeople));
                    }
                  }
                  if (value == 2) {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return CustomDialog(
                            title: "Delete People",
                            onPressed: () async {
                              peopleBloc.add(
                                  DeletePeopleEvent(widget.idPeople));
                            },
                          );
                        });
                  }
                },
                itemBuilder: (context) =>
                [
                  const PopupMenuItem(
                    child: Text("Update"),
                    value: 1,
                  ),
                  const PopupMenuItem(
                    child: Text("Delete"),
                    value: 2,
                  )
                ],
                icon: const Icon(Icons.more_vert),
              ),
            ],
          ),
          body: BlocBuilder<PeopleBloc, PeopleState>(
            builder: (context, state) {
              if (state is PeopleLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is PeopleLoaded) {
                return Container(
                  padding: const EdgeInsets.all(16),
                  child: ListView(
                    children: [
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          title: const Text("Name"),
                          subtitle: Text(state.people.name),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          title: const Text("Height"),
                          subtitle: Text(state.people.height),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          title: const Text("Mass"),
                          subtitle: Text(state.people.mass),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          title: const Text("Hair Color"),
                          subtitle: Text(state.people.hairColor),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          title: const Text("Skin Color"),
                          subtitle: Text(state.people.skinColor),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          title: const Text("Eye Color"),
                          subtitle: Text(state.people.eyeColor),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          title: const Text("Birth Year"),
                          subtitle: Text(state.people.birthYear),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          onTap: (){
                            Navigator.pushNamed(context, RouteList.listFilmsPeople, arguments: state.people.films);
                          },
                          title: const Text("Films"),
                          subtitle: const Text("Detail films"),
                          trailing: const Icon(Icons.arrow_forward_ios),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child:  ListTile(
                          onTap: (){
                            Navigator.pushNamed(context, RouteList.listSpeciesPeople, arguments: state.people.species);
                          },
                          title: const Text("Species"),
                          subtitle: const Text("Detail Species"),
                          trailing: const Icon(Icons.arrow_forward_ios),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          title: const Text("Created"),
                          subtitle: Text(DateFormat('dd MMMM yyyy HH:mm:ss')
                              .format(state.people.created)),
                        ),
                      ),
                      Container(
                        decoration: borderBottom(),
                        child: ListTile(
                          title: const Text("Updated"),
                          subtitle: Text(DateFormat('dd MMMM yyyy HH:mm:ss')
                              .format(state.people.edited)),
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  BoxDecoration borderBottom() {
    return BoxDecoration(
        border:
        Border(bottom: BorderSide(color: Colors.grey.withOpacity(0.2))));
  }
}
