import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/people/people_bloc.dart';
import 'package:test_majoo/presentation/themes/themes.dart';
import 'package:test_majoo/presentation/widgets/custom_dialog.dart';
import 'package:test_majoo/presentation/widgets/label_field_widget.dart';

class UpdatePeopleView extends StatefulWidget {
  final int idPeople;

  const UpdatePeopleView({Key? key, required this.idPeople}) : super(key: key);

  @override
  State<UpdatePeopleView> createState() => _UpdatePeopleViewState();
}

class _UpdatePeopleViewState extends State<UpdatePeopleView> {
  late final PeopleBloc peopleBloc;
  GlobalKey<FormState> globalKey = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    peopleBloc = getItInstance<PeopleBloc>();
    peopleBloc.add(InitDetailPeopleEvent(widget.idPeople));
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    peopleBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PeopleBloc>.value(
      value: peopleBloc,
      child: BlocListener<PeopleBloc, PeopleState>(
        listener: (context, state) {
          if (state is PeopleSuccess) {
            Navigator.pop(context, 'berhasil');
            Navigator.pop(context, 'berhasil');
            EasyLoading.showSuccess("Berhasil mengubah data");
          }
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Update People"),
          ),
          body: BlocBuilder<PeopleBloc, PeopleState>(
            builder: (context, state) {
              if(state is PeopleLoading){
                return const Center(child: CircularProgressIndicator(),);
              }else if(state is PeopleLoaded){
                return Container(
                  padding: const EdgeInsets.all(16),
                  child: Form(
                    key: globalKey,
                    child: ListView(
                      children: [
                        LabelFieldWidget(
                          label: "Name",
                          hintText: "Masukkan name",
                          initialValue: peopleBloc.peopleUpdate.name,
                          onChange: (value) {
                            peopleBloc.peopleUpdate =  peopleBloc.peopleUpdate.copy(
                                name: value
                            );
                          },
                        ),
                        LabelFieldWidget(
                          label: "Height",
                          hintText: "Masukkan height (ex:123)",
                          initialValue: peopleBloc.peopleUpdate.height,
                          onChange: (value) {
                            peopleBloc.peopleUpdate =  peopleBloc.peopleUpdate.copy(
                                height: value
                            );
                          },
                        ),
                        LabelFieldWidget(
                          label: "Mass",
                          hintText: "Masukkan mass (ex:192)",
                          initialValue: peopleBloc.peopleUpdate.mass,
                          onChange: (value) {
                            peopleBloc.peopleUpdate =  peopleBloc.peopleUpdate.copy(
                                mass: value
                            );
                          },
                        ),
                        LabelFieldWidget(
                          label: "Hair Color",
                          hintText: "Masukkan hair color",
                          initialValue: peopleBloc.peopleUpdate.hairColor,
                          onChange: (value) {
                            peopleBloc.peopleUpdate =  peopleBloc.peopleUpdate.copy(
                                hairColor: value
                            );
                          },
                        ),
                        LabelFieldWidget(
                          label: "Skin Color",
                          hintText: "Masukkan skin color",
                          initialValue: peopleBloc.peopleUpdate.skinColor,
                          onChange: (value) {
                            peopleBloc.peopleUpdate =  peopleBloc.peopleUpdate.copy(
                                skinColor: value
                            );
                          },
                        ),
                        LabelFieldWidget(
                          label: "Eye Color",
                          hintText: "Masukkan eye color",
                          initialValue: peopleBloc.peopleUpdate.eyeColor,
                          onChange: (value) {
                            peopleBloc.peopleUpdate =  peopleBloc.peopleUpdate.copy(
                                eyeColor: value
                            );
                          },
                        ),
                        LabelFieldWidget(
                          label: "Birth Year",
                          hintText: "Masukkan birth year",
                          initialValue: state.people.birthYear,
                          onChange: (value) {
                            peopleBloc.peopleUpdate =  peopleBloc.peopleUpdate.copy(
                                birthYear: value
                            );
                          },
                        ),
                        LabelFieldWidget(
                          label: "Gender",
                          hintText: "Masukkan gender (ex:male/female)",
                          initialValue: state.people.gender,
                          onChange: (value) {
                            peopleBloc.peopleUpdate =  peopleBloc.peopleUpdate.copy(
                                gender: value
                            );
                          },
                        ),
                        vSpace(16),
                        FractionallySizedBox(
                          widthFactor: 1,
                          child: ElevatedButton(
                            child: Text("SIMPAN", style: whiteTextStyle,),
                            onPressed: () {
                              FocusScope.of(context).unfocus();
                              if(globalKey.currentState!.validate()){
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return CustomDialog(
                                        title: "Update People",
                                        onPressed: () async {
                                          peopleBloc.add(
                                              UpdatePeopleEvent(peopleBloc.peopleUpdate));
                                        },
                                      );
                                    });
                              }
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }
}
