import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/common/utils/convert_handler.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/dashboard/dashboard_bloc.dart';
import 'package:test_majoo/presentation/blocs/favorit/favorit_bloc.dart';
import 'package:test_majoo/presentation/themes/themes.dart';

class FavoritView extends StatefulWidget {
  const FavoritView({Key? key}) : super(key: key);

  @override
  State<FavoritView> createState() => _FavoritViewState();
}

class _FavoritViewState extends State<FavoritView> {
  late FavoritBloc favoritBloc;

  @override
  void initState() {
    super.initState();
    favoritBloc = getItInstance<FavoritBloc>();
    favoritBloc.add(InitFavoritEvent());
  }

  @override
  void dispose() {
    super.dispose();
    favoritBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FavoritBloc>.value(
      value: favoritBloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Favorite Peoples",
            style: whiteTextStyle.copyWith(fontSize: 18, fontWeight: bold),
          ),
        ),
        body: BlocBuilder<FavoritBloc, FavoritState>(
          builder: (context, state) {
            if(state is DashboardLoading){
              return const Center(child:  CircularProgressIndicator(),);
            }else if(state is FavoritSuccess){
              return ListView.builder(
                itemCount: state.listPeople.length,
                itemBuilder: (context, index){
                  return ListTile(
                    leading: Container(
                      width: 40,
                      height: 40,
                      color: Colors.grey,
                      child: Center(child: Text(
                        ConvertHandlers().getInitials(state.listPeople[index].name),
                        style: whiteTextStyle.copyWith(fontSize: 18),)),
                    ),
                    title: Text(state.listPeople[index].name),
                    trailing: IconButton(
                      icon: const Icon(Icons.favorite, color: Colors.red,),
                      onPressed: () {
                        favoritBloc.add(DeleteFavoritEvent(state.listPeople[index].id!));
                      },
                    ),
                  );
                },
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
