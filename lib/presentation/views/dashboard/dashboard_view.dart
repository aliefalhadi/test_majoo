import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_majoo/common/constants/route_constants.dart';
import 'package:test_majoo/common/utils/convert_handler.dart';
import 'package:test_majoo/data/models/favorit_model.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/dashboard/dashboard_bloc.dart';
import 'package:test_majoo/presentation/themes/themes.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  State<DashboardView> createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  late DashboardBloc dashboardBloc;

  @override
  void initState() {
    super.initState();
    dashboardBloc = getItInstance<DashboardBloc>();
    dashboardBloc.add(InitDashboardEvent());
  }

  @override
  void dispose() {
    super.dispose();
    dashboardBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DashboardBloc>.value(
      value: dashboardBloc,
      child: Scaffold(
        appBar: AppBar(

          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(55),
            child: Column(
              children: [
                TextField(
                  decoration: inputDecoration.copyWith(
                    prefixIcon: const Icon(Icons.search),
                    hintText: "Search Name People"
                  ),
                  onChanged: (value){
                    dashboardBloc.add(SearchListDashboardEvent(value));
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(onPressed: (){
                      dashboardBloc.add(SortListDashboardEvent());
                    }, icon: const Icon(Icons.sort, color: Colors.white,)),
                    IconButton(onPressed: (){
                      dashboardBloc.add(const ChangeLayoutDashboardEvent(false));
                    }, icon: const Icon(Icons.grid_4x4_sharp, color: Colors.white)),
                    IconButton(onPressed: (){
                      dashboardBloc.add(const ChangeLayoutDashboardEvent(true));
                    }, icon: const Icon(Icons.list, color: Colors.white)),
                  ],
                )
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          onPressed: () async {
            var data =
                await Navigator.pushNamed(context, RouteList.createPeople);
            if (data != null) {
              dashboardBloc.add(InitDashboardEvent());
            }
          },
        ),
        body: BlocBuilder<DashboardBloc, DashboardState>(
          builder: (context, state) {
            if (state is DashboardLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (state is DashboardLoaded) {
              if(state.isLayoutList){
                return ListView.builder(
                  itemCount: state.lisPeople.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      onTap: () {
                        Navigator.pushNamed(context, RouteList.detailPeople,
                            arguments: state.lisPeople[index].id);
                      },
                      leading: Container(
                        width: 40,
                        height: 40,
                        color: Colors.grey,
                        child: Center(
                            child: Text(
                              ConvertHandlers()
                                  .getInitials(state.lisPeople[index].name),
                              style: whiteTextStyle.copyWith(fontSize: 18),
                            )),
                      ),
                      title: Text(state.lisPeople[index].name),
                      trailing: buildFavorit(
                        people: state.lisPeople[index],
                        favorit: state.listFavorit,
                      ),
                    );
                  },
                );
              }else{
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ListView(
                    children: [
                      Wrap(
                        spacing: 16,
                        runSpacing: 8,
                        children:List.generate(state.lisPeople.length, (index){
                          return Card(
                            child: SizedBox(
                              width: 160,
                              height: 150,
                              child: Column(
                                children: [
                                  Container(
                                    width: 180,
                                    height: 50,
                                    color: Colors.grey,
                                    child: Center(child:  Text(ConvertHandlers()
                                        .getInitials(state.lisPeople[index].name),style: whiteTextStyle.copyWith(fontSize: 18))),
                                  ),
                                  vSpace(16),
                                  Center(
                                    child: Text(state.lisPeople[index].name, style: blackTextStyle, maxLines: 1, textAlign: TextAlign.center,),
                                  ),
                                  vSpace(8),
                                  buildFavorit(
                                    people: state.lisPeople[index],
                                    favorit: state.listFavorit,
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                      ),
                    ],
                  ),
                );
              }
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class buildFavorit extends StatelessWidget {
  final People people;
  final List<Favorit> favorit;
  const buildFavorit({Key? key, required this.people, required this.favorit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    int index = favorit.indexWhere((element) => element.idPeople == people.id);
    if (index > -1) {
      return IconButton(
          onPressed: () {
            context
                .read<DashboardBloc>()
                .add(DeleteFavoritDashboardEvent(favorit[index].idPeople));
          },
          icon: const Icon(Icons.favorite, color: Colors.redAccent,));
    }
    return IconButton(
        onPressed: () {
          context
              .read<DashboardBloc>()
              .add(FavoritDashboardEvent(people.id.toString()));
        },
        icon: const Icon(Icons.favorite_border));
  }
}
