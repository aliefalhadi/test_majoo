import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:test_majoo/common/constants/route_constants.dart';
import 'package:test_majoo/di/get_it.dart';
import 'package:test_majoo/presentation/blocs/login/login_bloc.dart';
import 'package:test_majoo/presentation/themes/themes.dart';
import 'package:test_majoo/presentation/widgets/label_field_widget.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  late LoginBloc loginBloc;
  GlobalKey<FormState> globalKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    loginBloc = getItInstance<LoginBloc>();
  }

  @override
  void dispose() {
    super.dispose();
    loginBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginBloc>.value(
      value: loginBloc,
      child: BlocConsumer<LoginBloc, LoginState>(
        listener: (context, state) {
          if(state is LoginError){
            EasyLoading.showError("User tidak ditemukan");
          }else if(state is LoginSuccess){
            Navigator.pushNamedAndRemoveUntil(context, RouteList.home, (route) => false);
          }
        },
        builder: (context, state) {
            return Form(
              key: globalKey,
              child: Column(
                children: [
                  LabelFieldWidget(
                    label: "Username",
                    hintText: "Masukkan username",
                    onChange: (value) {
                      loginBloc.loginEntity.username = value;
                    },
                  ),
                  LabelFieldWidget(
                    isPasswordField: true,
                    label: "Password",
                    hintText: "Masukkan password",
                    onChange: (value) {
                      loginBloc.loginEntity.password = value;
                    },
                  ),
                  vSpace(16),
                  FractionallySizedBox(
                    widthFactor: 1,
                    child: ElevatedButton(
                      child: Text(
                        "Masuk",
                        style: whiteTextStyle,
                      ),
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        if(globalKey.currentState!.validate()){
                          loginBloc.add(LoginInitiateEvent(loginBloc.loginEntity));
                        }
                      },
                    ),
                  )
                ],
              ),
            );
        },
      ),
    );
  }
}
