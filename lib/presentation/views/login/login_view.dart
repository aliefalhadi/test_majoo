import 'package:flutter/material.dart';
import 'package:test_majoo/common/constants/route_constants.dart';
import 'package:test_majoo/presentation/themes/themes.dart';
import 'package:test_majoo/presentation/views/login/login_form.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: ListView(
          children: [
            Text("Login", style: blackTextStyle.copyWith(
              fontSize: 24, fontWeight: bold
            ),),
            vSpace(8),
            Text("Silahkan username dan password yang telah didaftarkan", style: blackTextStyle.copyWith(
              fontWeight: light
            ),),
            vSpace(16),
            const LoginForm(),
            vSpace(16),
            FractionallySizedBox(
              widthFactor: 1,
              child: OutlinedButton(
                child: Text("Belum ada akun? Daftar dulu", style: primaryTextStyle,),
                onPressed: (){
                  Navigator.pushNamed(context, RouteList.register);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
