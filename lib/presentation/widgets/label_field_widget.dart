import 'package:flutter/material.dart';
import 'package:test_majoo/presentation/themes/themes.dart';


class LabelFieldWidget extends StatelessWidget {
  final Key? textFieldKey;
  final String label;
  final String? initialValue;
  final String hintText;
  final bool isPasswordField;
  final TextEditingController? controller;
  final Function (String)? onChange;
  final String? Function(String?)? validator;
  final TextInputType typeInput;

  const LabelFieldWidget({
    Key? key,
    required this.label,
    this.initialValue,
    required this.hintText,
    this.controller,
    this.isPasswordField = false,
    this.typeInput = TextInputType.text,
    this.textFieldKey,
    this.onChange,
    this.validator
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label.toUpperCase(),
            style: blackTextStyle.copyWith(fontWeight: semiBold),
            textAlign: TextAlign.start,
          ),
          vSpace(8),
          TextFormField(
            key: textFieldKey,
            obscureText: isPasswordField,
            initialValue: initialValue,
            keyboardType: typeInput,
            obscuringCharacter: '*',
            controller: controller,
            validator: validator ?? (value) => value!.isEmpty ? 'inputan tidak boleh kosong' : null,
            style: blackTextStyle.copyWith(fontSize: 12),
            onChanged: onChange,
            decoration: inputDecoration.copyWith(hintText: hintText),
          ),
        ],
      ),
    );
  }
}