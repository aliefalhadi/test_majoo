import 'package:flutter/material.dart';

class CustomDialog extends StatelessWidget {
  final VoidCallback onPressed;
  final String? title;
  final String? subTitle;
  final String? labelNo;
  final String? labelYes;
  const CustomDialog({Key? key, required this.onPressed,  this.title,  this.subTitle,this.labelNo = 'Batal',this.labelYes='Ya'}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return  AlertDialog(
      title: Text(
          title??"Konfirmasi"),
      content: Text(
          subTitle??"Apakah anda yakin?"),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.pop(
                context);
          },
          child: Text(
            labelNo!,
            style: const TextStyle(
                color:
                Colors.black),
          ),
        ),
        TextButton(
          onPressed: onPressed,
          child: Text(labelYes!),
        )
      ],
    );
  }
}