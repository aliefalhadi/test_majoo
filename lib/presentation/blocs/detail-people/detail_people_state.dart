part of 'detail_people_bloc.dart';

abstract class DetailPeopleState extends Equatable {
  const DetailPeopleState();
  @override
  List<Object> get props => [];
}

class DetailPeopleInitial extends DetailPeopleState {

}


class DetailPeopleLoading extends DetailPeopleState {

}

class DetailFilmPeopleSuccess extends DetailPeopleState {
  final DetailFilmPeopleModel detailFilmPeopleModel;

  const DetailFilmPeopleSuccess(this.detailFilmPeopleModel);

  @override
  List<Object> get props => [detailFilmPeopleModel];
}

class DetailSpeciesPeopleSuccess extends DetailPeopleState {
  final DetailSpeciesPeopleModel detailSpeciesPeopleModel;

  const DetailSpeciesPeopleSuccess(this.detailSpeciesPeopleModel);

  @override
  List<Object> get props => [detailSpeciesPeopleModel];
}

class DetailPeopleSuccess extends DetailPeopleState {
  final People people;

  const DetailPeopleSuccess(this.people);

  @override
  List<Object> get props => [people];
}