part of 'detail_people_bloc.dart';

abstract class DetailPeopleEvent extends Equatable {
  const DetailPeopleEvent();
}


class InitDetailPeopleEvent extends DetailPeopleEvent{
  final int idPeople;

  const InitDetailPeopleEvent(this.idPeople);
  @override
  // TODO: implement props
  List<Object?> get props => [idPeople];
}

class DetailFilmPeopleEvent extends DetailPeopleEvent{
  final String url;

  const DetailFilmPeopleEvent(this.url);
  @override
  // TODO: implement props
  List<Object?> get props => [url];
}

class DetailSpeciesPeopleEvent extends DetailPeopleEvent{
  final String url;

  const DetailSpeciesPeopleEvent(this.url);
  @override
  // TODO: implement props
  List<Object?> get props => [url];
}
