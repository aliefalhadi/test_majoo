import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_majoo/data/models/detail_film_people_model.dart';
import 'package:test_majoo/data/models/detail_species_people_model.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/usecases/get_detail_film_people_case.dart';
import 'package:test_majoo/domain/usecases/get_detail_people_case.dart';
import 'package:test_majoo/domain/usecases/get_detail_species_people_case.dart';

part 'detail_people_event.dart';
part 'detail_people_state.dart';

class DetailPeopleBloc extends Bloc<DetailPeopleEvent, DetailPeopleState> {
  final GetDetailPeopleCase getDetailPeopleCase;
  final GetDetailFilmPeopleCase getDetailFilmPeopleCase;
  final GetDetailSpeciesPeopleCase getDetailSpeciesPeopleCase;
  DetailPeopleBloc(this.getDetailPeopleCase, this.getDetailFilmPeopleCase, this.getDetailSpeciesPeopleCase) : super(DetailPeopleInitial()) {
    on<InitDetailPeopleEvent>((event, emit) async{
       emit(DetailPeopleLoading());

       final eitherRes = await getDetailPeopleCase(event.idPeople.toString());

       eitherRes.fold((l) => print(l), (r){
         print(r);
         emit(DetailPeopleSuccess(r));
       });

    });

    on<DetailFilmPeopleEvent>((event, emit) async{
      emit(DetailPeopleLoading());

      final eitherRes = await getDetailFilmPeopleCase(event.url.toString());

      eitherRes.fold((l) => print(l), (r){
        //print(r);
        emit(DetailFilmPeopleSuccess(r));
      });

    });


    on<DetailSpeciesPeopleEvent>((event, emit) async{
      emit(DetailPeopleLoading());

      final eitherRes = await getDetailSpeciesPeopleCase(event.url.toString());

      eitherRes.fold((l) => print(l), (r){
        //print(r);
        emit(DetailSpeciesPeopleSuccess(r));
      });

    });
  }
}
