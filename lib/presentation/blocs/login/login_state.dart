part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class LoginInitial extends LoginState {
  const LoginInitial();

  @override
  List<Object> get props => [];
}

class LoginError extends LoginState {
  final String type;
  final String message;

  const LoginError(this.type, {this.message = ''});

  @override
  List<Object> get props => [type, message];
}

class LoginSuccess extends LoginState {
  @override
  List<Object?> get props => [];
}
