import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:test_majoo/common/constants/get_error_message.dart';
import 'package:test_majoo/domain/entities/login_entity.dart';
import 'package:test_majoo/domain/usecases/login_user_case.dart';
import 'package:test_majoo/presentation/blocs/loading/loading_bloc.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoadingBloc loadingBloc;
  final LoginUserCase loginUserCase;
  late LoginEntity loginEntity = LoginEntity(username: '', password: '');

  LoginBloc(this.loadingBloc, this.loginUserCase) : super(const LoginInitial()) {
    on<LoginInitiateEvent>((event, emit) async{
        loadingBloc.add(StartLoading());
        emit(const LoginInitial());
        print(event.loginEntity.password);
        final eitherRes = await loginUserCase(event.loginEntity);

       emit(eitherRes.fold((l){
          var type = getErrorMessage(l.appErrorType);
          debugPrint(l.appErrorType.toString());
          return LoginError(type, message: l.message);
        }, (r) => LoginSuccess()));

        loadingBloc.add(FinishLoading());
    });
  }
}
