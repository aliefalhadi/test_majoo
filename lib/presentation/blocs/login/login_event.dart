part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class LoginInitiateEvent extends LoginEvent{
  final LoginEntity loginEntity;

  const LoginInitiateEvent(this.loginEntity);

  @override
  List<Object?> get props => [loginEntity];
}
