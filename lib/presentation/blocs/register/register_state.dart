part of 'register_bloc.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();
}

class RegisterInitial extends RegisterState {
  const RegisterInitial();
  @override
  List<Object> get props => [];
}

class RegisterError extends RegisterState {
  final String type;
  final String message;

  const RegisterError(this.type, {this.message = ''});

  @override
  List<Object> get props => [type, message];
}

class RegisterSuccess extends RegisterState {
  @override
  List<Object?> get props => [];
}