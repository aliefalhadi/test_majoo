import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:test_majoo/common/constants/get_error_message.dart';
import 'package:test_majoo/domain/entities/app_error.dart';
import 'package:test_majoo/domain/entities/register_entity.dart';
import 'package:test_majoo/domain/usecases/register_user_case.dart';
import 'package:test_majoo/presentation/blocs/loading/loading_bloc.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final LoadingBloc loadingBloc;
  final RegisterUserCase registerUserCase;
  final RegisterEntity registerEntity = RegisterEntity(username: '', email: '', password: '');
  RegisterBloc(this.loadingBloc, this.registerUserCase) : super(RegisterInitial()) {
    on<InitiateRegisterEvent>((event, emit) async{
      loadingBloc.add(StartLoading());
      print(event.registerEntity.toJson());
      final Either<AppError, bool> eitherRes = await registerUserCase(event.registerEntity);

      emit(eitherRes.fold((l){
        var type = getErrorMessage(l.appErrorType);
        debugPrint(l.appErrorType.toString());
        return RegisterError(type, message: l.message);
      }, (r) => RegisterSuccess()));

      loadingBloc.add(FinishLoading());
    });

  }
}
