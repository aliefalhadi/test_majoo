part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}


class InitiateRegisterEvent extends RegisterEvent{
  final RegisterEntity registerEntity;

  const InitiateRegisterEvent(this.registerEntity);

  @override
  List<Object?> get props => [registerEntity];
}
