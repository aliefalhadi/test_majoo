part of 'favorit_bloc.dart';

abstract class FavoritState extends Equatable {
  const FavoritState();
}

class FavoritInitial extends FavoritState {
  @override
  List<Object> get props => [];
}

class FavoritNull extends FavoritState {
  @override
  List<Object> get props => [];
}

class FavoritLoading extends FavoritState {
  @override
  List<Object> get props => [];
}

class FavoritSuccess extends FavoritState {
  final List<People> listPeople;

  const FavoritSuccess(this.listPeople);

  FavoritSuccess copyWith({List<People>? listPeople}) {
    return FavoritSuccess(
      listPeople ?? this.listPeople,
    );
  }

  @override
  List<Object> get props => [listPeople];
}
