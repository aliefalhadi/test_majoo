import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/usecases/delete_favorit_case.dart';
import 'package:test_majoo/domain/usecases/get_list_favorit_people_case.dart';

part 'favorit_event.dart';
part 'favorit_state.dart';

class FavoritBloc extends Bloc<FavoritEvent, FavoritState> {
  final GetListFavoritPeopleCase getListFavoritPeopleCase;
  final DeleteFavoritCase deleteFavoritCase;
  FavoritBloc(this.getListFavoritPeopleCase, this.deleteFavoritCase) : super(FavoritInitial()) {
    on<InitFavoritEvent>((event, emit) async{
      emit(FavoritLoading());

      final eitherRes = await getListFavoritPeopleCase();

      eitherRes.fold((l) => null, (r){
        if(r.isEmpty){
         emit(FavoritNull());
        }else{
          emit(FavoritSuccess(r));
        }
      });
    });

    on<DeleteFavoritEvent>((event, emit) async {
      final stateCurr = state;
      if (stateCurr is FavoritSuccess) {
        emit(FavoritLoading());

       await deleteFavoritCase(event.idFavorit);

        stateCurr.listPeople.removeWhere((element) => element.id == event.idFavorit);

        emit(stateCurr.copyWith(listPeople: stateCurr.listPeople));

      }
    });
  }
}
