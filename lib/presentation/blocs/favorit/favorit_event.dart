part of 'favorit_bloc.dart';

abstract class FavoritEvent extends Equatable {
  const FavoritEvent();
}

class InitFavoritEvent extends FavoritEvent{
  @override
  List<Object?> get props => [];
}

class DeleteFavoritEvent extends FavoritEvent{
  final int idFavorit;

  const DeleteFavoritEvent(this.idFavorit);
  @override
  List<Object?> get props => [idFavorit];
}