part of 'dashboard_bloc.dart';

abstract class DashboardEvent extends Equatable {
  const DashboardEvent();
}


class InitDashboardEvent extends DashboardEvent{
  @override
  List<Object?> get props => [];
}

class ChangeLayoutDashboardEvent extends DashboardEvent{
  final bool isLayoutList;

  const ChangeLayoutDashboardEvent(this.isLayoutList);
  @override
  List<Object?> get props => [isLayoutList];
}

class SortListDashboardEvent extends DashboardEvent{
  @override
  List<Object?> get props => [];
}

class SearchListDashboardEvent extends DashboardEvent{
  final String search;

  const SearchListDashboardEvent(this.search);
  @override
  List<Object?> get props => [search];
}

class FavoritDashboardEvent extends DashboardEvent{
  final String idPeople;

  const FavoritDashboardEvent(this.idPeople);
  @override
  List<Object?> get props => [idPeople];
}

class DeleteFavoritDashboardEvent extends DashboardEvent{
  final int idFavorit;

  const DeleteFavoritDashboardEvent(this.idFavorit);
  @override
  List<Object?> get props => [idFavorit];
}
