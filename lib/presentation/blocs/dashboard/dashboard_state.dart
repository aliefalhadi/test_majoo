part of 'dashboard_bloc.dart';

abstract class DashboardState extends Equatable {
  const DashboardState();
  @override
  List<Object> get props => [];
}

class DashboardInitial extends DashboardState {}

class DashboardLoading extends DashboardState {}

class DashboardLoaded extends DashboardState {
  final List<People> lisPeople;
  final List<Favorit> listFavorit;
  final bool isLayoutList;

  const DashboardLoaded({required this.lisPeople, required this.listFavorit, required this.isLayoutList});

  DashboardLoaded copyWith({List<People>? lisPeople, List<Favorit>? listFavorit, bool? isLayoutList}) {
    return DashboardLoaded(
      lisPeople: lisPeople ?? this.lisPeople,
      listFavorit: listFavorit ?? this.listFavorit,
      isLayoutList: isLayoutList ?? this.isLayoutList,
    );
  }

  @override
  List<Object> get props => [lisPeople];
}

class DashboardError extends DashboardState {
  final String type;
  final String message;

  const DashboardError(this.type, {this.message = ''});

  @override
  List<Object> get props => [type, message];
}
