import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_majoo/common/constants/get_error_message.dart';
import 'package:test_majoo/data/models/favorit_model.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/usecases/create_favorit_case.dart';
import 'package:test_majoo/domain/usecases/delete_favorit_case.dart';
import 'package:test_majoo/domain/usecases/get_list_favorit_case.dart';
import 'package:test_majoo/domain/usecases/get_list_people_case.dart';
import 'package:test_majoo/domain/usecases/update_people_case.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  final GetListPeopleCase getListPeopleCase;
  final GetListFavoritCase getListFavoritCase;
  final CreateFavoritCase createFavoritCase;
  final UpdatePeopleCase updatePeopleCase;
  final DeleteFavoritCase deleteFavoritCase;

  String sort = "ASC";
  late List<People> listPeopleMaster;

  DashboardBloc(
      this.getListPeopleCase, this.updatePeopleCase, this.getListFavoritCase, this.createFavoritCase, this.deleteFavoritCase)
      : super(DashboardInitial()) {
    on<InitDashboardEvent>((event, emit) async {
      emit(DashboardLoading());

      final eitherRes = await getListPeopleCase();

      eitherRes.fold((l) {
        var type = getErrorMessage(l.appErrorType);
        debugPrint(l.appErrorType.toString());
        return emit(DashboardError(type, message: l.message));
      }, (r) => listPeopleMaster = r);

      final eitherResFavorit = await getListFavoritCase();

      eitherResFavorit.fold((l) {
        var type = getErrorMessage(l.appErrorType);
        debugPrint(l.appErrorType.toString());
        return emit(DashboardError(type, message: l.message));
      }, (r) {
        return emit(DashboardLoaded(lisPeople: listPeopleMaster, listFavorit: r,isLayoutList: true));
      });
    });

    on<FavoritDashboardEvent>((event, emit) async {
      final stateCurr = state;
      if (stateCurr is DashboardLoaded) {
        emit(DashboardLoading());

        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        String? idUser = sharedPreferences.getString('id');

        Favorit favorit = Favorit(idUser: int.parse(idUser!), idPeople: int.parse(event.idPeople));

        final eitherRes = await createFavoritCase(favorit);

        eitherRes.fold((l) => null, (r){
         favorit = favorit.copy(id: r);
        });

        stateCurr.listFavorit.add(favorit);

        emit(stateCurr.copyWith(listFavorit: stateCurr.listFavorit));

      }
    });

    on<ChangeLayoutDashboardEvent>((event, emit) async {
      final stateCurr = state;
      if (stateCurr is DashboardLoaded) {
        emit(DashboardLoading());

        emit(stateCurr.copyWith(isLayoutList: event.isLayoutList));

      }
    });

    on<SearchListDashboardEvent>((event, emit) async {
      final stateCurr = state;
      if (stateCurr is DashboardLoaded) {
        emit(DashboardLoading());

        late List<People> listTemp = [];
        for (var data in listPeopleMaster) {
          if(data.name.toLowerCase().contains(event.search)){
            listTemp.add(data);
          }
        }

        emit(stateCurr.copyWith(lisPeople: listTemp));

      }
    });


    on<SortListDashboardEvent>((event, emit) async {
      final stateCurr = state;
      if (stateCurr is DashboardLoaded) {
        emit(DashboardLoading());

        if(sort == "ASC"){
          sort = "DESC";
          stateCurr.lisPeople.sort((a,b){
            return a.name.toLowerCase().compareTo(b.name.toLowerCase());
          });
          emit(stateCurr.copyWith(lisPeople: stateCurr.lisPeople));
        }else{
          sort = "ASC";
          stateCurr.lisPeople.sort((a,b){
            return b.name.toLowerCase().compareTo(a.name.toLowerCase());
          });
          emit(stateCurr.copyWith(lisPeople: stateCurr.lisPeople));
        }
      }
    });

    on<DeleteFavoritDashboardEvent>((event, emit) async {
      final stateCurr = state;
      if (stateCurr is DashboardLoaded) {
        emit(DashboardLoading());

        deleteFavoritCase(event.idFavorit);

        stateCurr.listFavorit.removeWhere((element) => element.idPeople == event.idFavorit);

        emit(stateCurr.copyWith(listFavorit: stateCurr.listFavorit));

      }
    });
  }
}
