part of 'people_bloc.dart';

abstract class PeopleState extends Equatable {
  const PeopleState();
  @override
  List<Object> get props => [];
}

class PeopleInitial extends PeopleState {

}

class PeopleLoading extends PeopleState {

}

class PeopleLoaded extends PeopleState {
  final People people;

  const PeopleLoaded(this.people);

  @override
  List<Object> get props => [people];
}


class PeopleSuccess extends PeopleState {

}

class PeopleDeleted extends PeopleState {

}
