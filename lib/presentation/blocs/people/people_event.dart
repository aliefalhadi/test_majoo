part of 'people_bloc.dart';

abstract class PeopleEvent extends Equatable {
  const PeopleEvent();
}

class CreatePeopleEvent extends PeopleEvent{
  @override
  List<Object?> get props => [];
}

class DeletePeopleEvent extends PeopleEvent{
  final int idPeople;

  const DeletePeopleEvent(this.idPeople);
  @override
  List<Object?> get props => [];
}

class DetailPeopleEvent extends PeopleEvent{
  final int idPeople;

  const DetailPeopleEvent(this.idPeople);
  @override
  List<Object?> get props => [idPeople];

}

class InitDetailPeopleEvent extends PeopleEvent{
  final int idPeople;

  const InitDetailPeopleEvent(this.idPeople);
  @override
  List<Object?> get props => [idPeople];

}

class UpdatePeopleEvent extends PeopleEvent{
  final People people;

  const UpdatePeopleEvent(this.people);
  @override
  List<Object?> get props => [people];
}
