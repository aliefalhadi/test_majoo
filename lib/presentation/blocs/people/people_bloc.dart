import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/domain/entities/people_entity.dart';
import 'package:test_majoo/domain/usecases/create_people_case.dart';
import 'package:test_majoo/domain/usecases/delete_people_case.dart';
import 'package:test_majoo/domain/usecases/get_detail_people_case.dart';
import 'package:test_majoo/domain/usecases/update_people_case.dart';
import 'package:test_majoo/presentation/blocs/loading/loading_bloc.dart';

part 'people_event.dart';
part 'people_state.dart';

class PeopleBloc extends Bloc<PeopleEvent, PeopleState> {
  final LoadingBloc loadingBloc;
  final CreatePeopleCase createPeopleCase;
  final UpdatePeopleCase updatePeopleCase;
  final DeletePeopleCase deletePeopleCase;
  final GetDetailPeopleCase getDetailPeopleCase;

  late People peopleUpdate;

  PeopleEntity peopleEntity = PeopleEntity(
      name: '',
      height: '',
      mass: '',
      hairColor: '',
      skinColor: '',
      eyeColor: '',
      birthYear: '',
      gender: '');
  PeopleBloc(this.loadingBloc, this.createPeopleCase, this.deletePeopleCase,
      this.getDetailPeopleCase, this.updatePeopleCase)
      : super(PeopleInitial()) {
    on<CreatePeopleEvent>((event, emit) async {
      loadingBloc.add(StartLoading());

      People people = People(
          name: peopleEntity.name,
          height: peopleEntity.height,
          mass: peopleEntity.mass,
          hairColor: peopleEntity.hairColor,
          skinColor: peopleEntity.skinColor,
          eyeColor: peopleEntity.eyeColor,
          birthYear: peopleEntity.birthYear,
          gender: peopleEntity.gender,
          films: [
            "https://swapi.dev/api/films/1/",
            "https://swapi.dev/api/films/2/",
            "https://swapi.dev/api/films/3/",
            "https://swapi.dev/api/films/6/"
          ],
          species: ["https://swapi.dev/api/species/2/"],
          url: "",
          created: DateTime.now(),
          edited: DateTime.now());

      final eitherRes = await createPeopleCase(people);

      eitherRes.fold((l) => print(l), (r) => emit(PeopleSuccess()));

      loadingBloc.add(FinishLoading());
    });

    on<DeletePeopleEvent>((event, emit) async {
      loadingBloc.add(StartLoading());

      final eitherRes = await deletePeopleCase(event.idPeople);

      eitherRes.fold((l) => print(l), (r) => emit(PeopleDeleted()));

      loadingBloc.add(FinishLoading());
    });

    on<DetailPeopleEvent>((event, emit) async {
      emit(PeopleLoading());

      final eitherRes = await getDetailPeopleCase(event.idPeople.toString());

      eitherRes.fold((l) => print(l), (r) {
        emit(PeopleLoaded(r));
      });
    });

    on<InitDetailPeopleEvent>((event, emit) async {
      emit(PeopleLoading());

      final eitherRes = await getDetailPeopleCase(event.idPeople.toString());

      eitherRes.fold((l) => print(l), (r) {
        peopleUpdate = r;
        emit(PeopleLoaded(r));
      });
    });

    on<UpdatePeopleEvent>((event, emit) async {
      loadingBloc.add(StartLoading());



      final eitherRes = await updatePeopleCase(event.people);

      eitherRes.fold((l) => print(l), (r) {
        emit(PeopleSuccess());
      });
      loadingBloc.add(FinishLoading());
    });
  }
}
