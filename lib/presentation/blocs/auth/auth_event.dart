part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
  @override
  List<Object> get props => [];
}

class LogoutAuth extends AuthEvent {}

class LoadingAuth extends AuthEvent {}

class LoadedAuth extends AuthEvent {}
