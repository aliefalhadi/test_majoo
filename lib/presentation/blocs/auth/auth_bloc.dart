import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_majoo/data/models/user_model.dart';
import 'package:test_majoo/domain/usecases/get_user_login_case.dart';
import 'package:test_majoo/domain/usecases/logout_user_case.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final GetUserLoginCase getUserLoginCase;
  final LogoutUserCase logoutUserCase;
  AuthBloc(this.getUserLoginCase, this.logoutUserCase) : super(AuthInitial()) {
    on<AuthEvent>((event, emit) async {
      emit(AuthLoading());
      final user = await getUserLoginCase();
      emit(AuthLoaded(user));
    });

    on<LogoutAuth>((event, emit) async {
      await logoutUserCase();
      emit(AuthLogoutSuccess());
    });
  }
}
