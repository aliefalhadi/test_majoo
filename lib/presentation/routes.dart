import 'package:flutter/material.dart';
import 'package:test_majoo/common/constants/route_constants.dart';
import 'package:test_majoo/data/models/people_model.dart';
import 'package:test_majoo/presentation/views/login/login_view.dart';
import 'package:test_majoo/presentation/views/people/create_people_view.dart';
import 'package:test_majoo/presentation/views/people/detail_film_people_view.dart';
import 'package:test_majoo/presentation/views/people/detail_people_view.dart';
import 'package:test_majoo/presentation/views/people/detail_species_people_view.dart';
import 'package:test_majoo/presentation/views/people/list_films_people.dart';
import 'package:test_majoo/presentation/views/people/list_species_people.dart';
import 'package:test_majoo/presentation/views/people/update_people_view.dart';
import 'package:test_majoo/presentation/views/register/register_view.dart';
import 'package:test_majoo/presentation/views/tabbed_home_view.dart';

class AppRouter {
  Route? onGenerateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case RouteList.home:
        return MaterialPageRoute(builder: (_) =>  const TabbedHomeWidget());
      case RouteList.login:
        return MaterialPageRoute(builder: (_) =>  const LoginView());
      case RouteList.register:
        return MaterialPageRoute(builder: (_) =>  const RegisterView());
      case RouteList.detailPeople:
        return MaterialPageRoute(builder: (_) =>  DetailPeopleView(
          idPeople: routeSettings.arguments as int,
        ));
      case RouteList.createPeople:
        return MaterialPageRoute(builder: (_) =>  const CreatePeopleView());
      case RouteList.updatePeople:
        return MaterialPageRoute(builder: (_) =>  UpdatePeopleView(
          idPeople: routeSettings.arguments as int,
        ));
      case RouteList.listFilmsPeople:
        return MaterialPageRoute(builder: (_) =>  ListFilmsPeople(
          films: routeSettings.arguments as List<String>,
        ));
      case RouteList.listSpeciesPeople:
        return MaterialPageRoute(builder: (_) =>  ListSpeciesPeople(
          species:  routeSettings.arguments as List<String>,
        ));

      case RouteList.detailFilmsPeople:
        return MaterialPageRoute(builder: (_) =>  DetailFilmPeopleView(
          url: routeSettings.arguments as String,
        ));
      case RouteList.detailSpeciesPeople:
        return MaterialPageRoute(builder: (_) =>  DetailSpeciesPeopleView(
          url: routeSettings.arguments as String,
        ));
      default:
        return null;
    }
  }
}