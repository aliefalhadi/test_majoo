import 'dart:ui';

class AppColor {
  const AppColor._();

  static const Color primary = Color(0xFF041E41);
  static const Color secondary = Color(0xFFF7C71E);
}